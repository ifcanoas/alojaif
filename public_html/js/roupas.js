$(document).ready(function(){
    $(".addRegistroRoupa").on("click", function(){
        let idPessoa = $(this).data('id');
        let idTipoRegistro = $(this).data('tipo');

        exibirTelaComplemento(`<h2>Adicionar uma entrenga de roupa</h2>
            <p class="help"> Todos os campos são opcionais </p>
            <label> Quantidade
            <input name="quantidade" id="qtdRoupa" type="number" class="form-control" value="1">
            </label> 
            <br />
            <label> Tamanho
            <input name="tamanho" class="form-control" type="text" />
            </label>
            <br />
            <label> Digite uma observação ou descrição do Registro
            <textarea name="observacao" class="form-control"></textarea>
            </label>
      
        `, idPessoa, idTipoRegistro, function(){

            $("#roupasRelatorio" + idPessoa).append(
                `
                    <li>${$('#qtdRoupa').val()} - ${tiposRegistro[idTipoRegistro].label} - ${now()}</li>
                `
            );
        })
    });


})