window.onbeforeunload = function () {
  window.scrollTo(0, 0);
};


function exibirTelaComplemento(template, id, tipo, callback = function () { }, rota = '/admin/registro/criarNovoFimAjax') {
  $("#modalGeral .modal-body").html(
    `
      <form id="formObservacaoTemp">
        <input type="hidden" name="abrigadoId" value="${id}" />
        <input type="hidden" name="tipoRegistroId" value="${tipo}" />
      
          ${template}
       
        <button type="submit" class="btn btn-primary">Confirmar</button>
      </form>
      `
  );
  $("#modalGeral").modal("show");
  $('#formObservacaoTemp').on('submit', function (e) {
    e.preventDefault();
    $.post(rota,
      $('#formObservacaoTemp').serialize(), function (data) {
        //ver quem esta usando esse callback para padronizar 
        callback(id, tipo, template, rota);
        /*  if(typeof callback === 'string'){
            eval(callback + `(${idPessoa}, ${idRegistro}, ${obs}, ${data});` );
          }
          else{
            callback(idPessoa, idRegistro, obs, data);
          }*/
      }
    )
    $("#modalGeral").modal("hide");

  });
}


function addRegistroBackground(idPessoa, idRegistro, obs = '', callback = function () { }) {

  $.post('/admin/registro/criarNovoFimAjax',
    `abrigadoId=${idPessoa}&tipoRegistroId=${idRegistro}&observacao=${obs}`,
    function (data) {
      if (typeof callback === 'string') {
        eval(callback + `(${idPessoa}, ${idRegistro}, ${obs}, ${data});`);
      }
      else {
        callback(idPessoa, idRegistro, obs, data);
      }
    }
  );
}

function addRegistro(idPessoa) {

  let combo = '';
  for (let a in tiposRegistro) {
    combo += `<option value="${a}">${tiposRegistro[a]}</option>`
  }
  exibirTelaComplemento(`<h2> Adicionar um registo</h2>
    <label>Selecione o tipo de Registro
      <select name="tipoRegistroId" class="form-control">
        ${combo}
      </select>
      </label>
      <br /><a href="/admin/tipoRegistro/criarNovo">Não encontrou o tipo de registro adequado</a>
      <hr>
      <label> Digite uma observação ou descrição do Registro
      <textarea name="observacao" class="form-control"></textarea>
  
    `, idPessoa, -1)
}

$(document).ready(function () {


  $(".addRegistro").on("click", function () {
    addRegistro($(this).data('id'))
  });

  $(".addRegistroBackground").on("click", function () {

    let idPessoa = $(this).data('id');
    let idTipoRegistro = $(this).data('tipo');
    let obs = $(this).data('tipo');
    let callback = $(this).data('callback');
    addRegistroBackground(idPessoa, idTipoRegistro, obs, callback)
  });

  $(".addRegistroTipo").on("click", function () {
    let idPessoa = $(this).data('id');
    let idTipoRegistro = $(this).data('tipo');
    let messageHelp = $(this).data('help') ?? '';
    exibirTelaComplemento(`<h2> Adicionar um registo</h2>
        <p class="help"> ${messageHelp} </p>
      <label> Digite uma observação ou descrição do Registro
      <textarea name="observacao" class="form-control"></textarea>
  
    `, idPessoa, idTipoRegistro)
  });

  window.onscroll = function () {
    onScroll();
  };


  // let scrolled = false;
  // let divCima
  // let thead
  // let sticky

  // function onScroll() {
  //   if (!scrolled) {
  //     const header = document.getElementById("header");
  //     headerHeight = header.offsetHeight;
  //     divCima = document.getElementById("divDeCima");
  //     sticky = divCima.offsetTop;
  //   }
  //   else {
  //     if (window.pageYOffset + headerHeight + 5 > sticky) {

  //       divCima.classList.add("sticky");
  //     } else {
  //       divCima.classList.remove("sticky");
  //     }
  //   }
  //   scrolled = true;
  // }


  const table = $('#tabelaAbrigados').DataTable();
  $("#divDeCima .selectEstado").on("change", function () {
    const select = $(this)[0];
    const className = select.options[select.selectedIndex].className;
    $(this).removeClass().addClass('selectEstado ' + className);
    table.search($('#search').val()).draw(); //importantissimo
    if (className != 'todos' && className != 'abrigadosGeral' && className != 'desocupacoesGeral') {
      $('#tabelaAbrigados tbody tr').hide();
      $(`#tabelaAbrigados tbody tr select.${className}`).parent().parent().show();
    }
    else {
      $('#tabelaAbrigados tbody tr').show();
    }


  })
  $('#search').keyup(function () {
    table.search($(this).val()).draw();
  });



  $('#tabelaAbrigados').removeClass('loading')
  $('#loader').removeClass('loading')
})

function showAbrigados() {
  $('#tabelaAbrigados tbody tr').show();
  $('#tabelaAbrigados tbody tr select.desocupou').parent().parent().hide();
  $('#tabelaAbrigados tbody tr select.obito').parent().parent().hide();
  $('#tabelaAbrigados tbody tr select.movido').parent().parent().hide();
  $('#tabelaAbrigados tbody tr select.teste').parent().parent().hide();
}

function showDesocupacoes() {
  $('#tabelaAbrigados tbody tr').hide();
  $('#tabelaAbrigados tbody tr select.desocupou').parent().parent().show();
  $('#tabelaAbrigados tbody tr select.obito').parent().parent().show();
  $('#tabelaAbrigados tbody tr select.movido').parent().parent().show();
  $('#tabelaAbrigados tbody tr select.teste').parent().parent().show();
}