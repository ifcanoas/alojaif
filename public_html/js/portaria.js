$(document).ready(function(){
    $.each($(".movido"), function (indexInArray, el) { 
        let id = $(el).attr('id').replace('status', '');
        $("#tr"+id + ' .btn').attr('disabled', true)
    });

    $('.btnSair').on('click', function(){
        let idPessoa =  $(this).data("id");
        let obs = 'Saída padrão';
        if($(this).data('situacao') == 2 ){
            $.get(
                "/admin/abrigado/atualizarSituacao/" + idPessoa+ "/1" ,
                function (data) {
                 
                }
            );
        }

        $.post('/admin/registro/criarNovoFimAjax',
        `abrigadoId=${idPessoa}&tipoRegistroId=22&observacao=${obs}`,
        function(data){
            console.log(data)
            $("#status" + idPessoa).html('SAÍDA TEMPORÁRIA')
            $("#status" + idPessoa).removeClass().addClass('badge saida')
        }
      )
    });

    $('.btnSaidaDefinitiva').on('click', function(){
        let idPessoa =  $(this).data("id");
        let obs = 'Saída definitiva \0/';
        
            $.get(
                "/admin/abrigado/atualizarSituacao/" + idPessoa+ "/3" ,
                function (data) {
                 
                }
            );
        

        $.post('/admin/registro/criarNovoFimAjax',
        `abrigadoId=${idPessoa}&tipoRegistroId=13&observacao=${obs}`,
        function(data){
            console.log(data)
            $("#status" + idPessoa).html('DESOCUPOU')
            $("#status" + idPessoa).removeClass().addClass('badge desocupou')
        }
      )
    });

    $('.btnEntrar').on('click', function(){
        let idPessoa =  $(this).data("id");
        let obs = 'Entrada padrão';
        $.get(
            "/admin/abrigado/atualizarSituacao/" + $(this).data("id") + "/2" ,
            function (data) {
                $("#status" + idPessoa).html('ABRIGADO')
                $("#status" + idPessoa).removeClass().addClass('badge abrigado')
                $.post('/admin/registro/criarNovoFimAjax',
                `abrigadoId=${idPessoa}&tipoRegistroId=23&observacao=${obs}`,
                function(data){
                    console.log(data)
                });
            }
        );
    });
})