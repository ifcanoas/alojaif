$(document).ready(function(){
    if(tutorID != -1){
        buscarTutor(tutorID)
        $("#buscaTutor").hide('slow');
        $('#buscaErro').hide()
    }
    

    $("#btnBuscaTutor").on('click', function(e){
        e.preventDefault();
        $("#buscaTutor").hide('slow');
        $('#buscaErro').hide()


        buscarTutor($("#inputTutor").val());
    });


    $("#btnRemoverTutor").on('click', function(e){
        e.preventDefault();
        $("#buscaTutor").show('slow');
        $("#telaTutorSelecionado").hide('slow');


        $("#inputTutor").val('');
    });

});

function buscarTutor(id){
    id = parseInt(id)
    id = isNaN(id)  ? 0 : id
    $.get('/admin/abrigado/load/'+ id, function(data){
        let abrigado = JSON.parse(data) 
        let tpl = ''
        if(abrigado.id){
            tpl = `
            <a href="/admin/abrigado/getFotoModal/${abrigado.id}"  class="modalGeral">
                <img src="/admin/abrigado/getMiniatura/${abrigado.id}">
            </a>
            <p>${abrigado.nome}</p>
            <input type="hidden" name="abrigadoId" value="${abrigado.id}" />
             `       
            $("#tutor").html(tpl);
            $("#telaTutorSelecionado").show('slow');
        }else{
           
            naoEncontrado();
        }

       
    }).fail(function (){
        naoEncontrado()
    })

    function naoEncontrado(){
        $('#buscaErro').html('Código não encontrado');
        $('#buscaErro').show();
        $("#buscaTutor").show('slow');

    }
}