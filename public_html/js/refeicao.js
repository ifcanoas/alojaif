function cadastrarRegistro(idPessoa, idEvento, texto, data, elementId){
    elementId = "#" + elementId
    const idRegistro = $(elementId).data("id");
    console.log(elementId);
    const deveRemover = idRegistro !== -1;
    if (deveRemover) {
      $.get(
        `/admin/registro/deletarFimAjax/${idRegistro}`,
        function(){
          $(elementId).data("id", -1)
        }
      )
    } else {
      $.post(
        '/admin/registro/criarNovoFimAjax',
        `abrigadoId=${idPessoa}&tipoRegistroId=${idEvento}&observacao=${texto}&dataRegistro=${data}`,
        function(data){
          data = JSON.parse(data)
          $(elementId).data("id", data.id)
        }
      )
    }
  }