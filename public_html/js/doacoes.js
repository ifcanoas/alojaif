$(document).ready(() => {
    $('#adicionarDoacao').on('click', function () {
        $("#modalGeral .modal-body").html($("#adicionarDoacaoTPL").html());
        $("#modalGeral").modal('show')
    
        buscaPessoaTela();
        
        $("#btnCancelar").on('click', 
            (e) => {        
                e.preventDefault();
                $("#modalGeral").modal('hide');    
            }
        ); 

        $("#formDoacao").on('submit', 
            (e) => {
                e.preventDefault();
                Eny.postForm('#formDoacao',
                    (data) =>{
                        console.log(data);
                        $('#tabelaDoacoesData').append(`
                        <tr>
                            <td>${now()}</td>
                            <td>${$("#nomeCompleto").val()}</td>
                            <td>${$("#cpf2").val()}</td>
                            <td>{$doacao->getTipoRegistro()->getTipo()}</td>
                            <td>Quantidade: 1 {$doacao->getObservacao()}</td>
                        </tr>
                        
                        `);
                        $("#modalGeral").modal('hide');    

                    }
                ,
                (dataError) =>{
                    console.log(dataError);
                

                }
                );
            }
        )
    
    });

 

});


function buscaPessoaTela(){
    $("#btnBuscaPessoa").on('click', function(e){
        e.preventDefault();
        $("#cpf").attr('disabled', true);

        buscaPessoa($("#cpf").val());
        $('#btnBuscaPessoa').hide()
        $("#btnRecomecarBusca").show('slow')
    });



}

function buscaPessoa(cpf)
{

    $.get('/admin/pessoa/getByCPF/' + cpf, function(data){
        let pessoa = JSON.parse(data);
        $('#pessoaId').val(pessoa.id);
        $('#nomeCompleto').val(pessoa.nomeCompleto)
        $("#messagePessoa").html('Pessoa já cadastrada se precisar atualize algum dado');
        showNextForm();
    }).fail(showNextForm);


    function showNextForm(){
        $("#cpf2").val($("#cpf").val());
        $("#btnRegistrar").attr('disabled', false);
        $("#formPessoa").show();
        $("#buscaPessoaTela").hide('slow');
    }
}