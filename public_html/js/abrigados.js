const pessoas = {}


$(document).ready(function () {




 $(".addVeiculo").on("click", function(){
  exibirTelaComplemento(`<h2> Adicionar um veículo a pessoa</h2>
    <label>Informe a placa do veículo
      <input name="observacao" type="text" class="form-control"/>
    </label>
  `, $(this).data('id'), 14)
 });

 $(".addResponsavel").on("click", function(){

    getResponsavel($(this).data('id'));


    function getResponsavel(idAbrigado){


      $("#modalGeral .modal-body").html(
        `<form id="buscarResponsavel">
          <input type="text" id="buscaResponsavel" class="form-control"> 
          <button type="submit" class="btn btn-primary">Buscar familiar</button>
        </form>
        `
      );
      $("#modalGeral").modal("show");
      $("#buscarResponsavel").on('submit', function(e){
        e.preventDefault();
        $.get('/admin/abrigado/load/'+$("#buscaResponsavel").val(), function(data){
          let combo = '';
          for(let a in tiposResponsaveis){
            combo += `<option value="${a}">${tiposResponsaveis[a]}</option>`
          }
          let responsavel = JSON.parse(data)
          $("#modalGeral .modal-body").html(
            `
            <form id="formResponsavel">
              <img src="/admin/abrigado/getMiniatura/${responsavel.id}">
              <p>${responsavel.nome}</p>
              <input type="hidden" name="responsavelId" value="${responsavel.id}" />
              <input type="hidden" name="abrigadoId" value="${idAbrigado}" />
              <label> Observações:
              <textarea class="form-control" name="observacao"></textarea>
              </label>

              <select name="tipoResponsavelId" class="form-control">
                ${combo}
              </select>
              <a href="/admin/tipoResponsavel/criarNovo">[+] Adicionar relação familiar </a>

              <hr />
              <div class="row">
                <div class="col">
                  <button type="submit" class="btn btn-primary">Adicionar relação </button>
                </div>
                <div class="col">
                  <button id="trocarResponsavel" class="btn btn-danger">Trocar código</button>
                </div>
              </div>
            </form>
            `
          );

          $("#trocarResponsavel").on('click', function(e){
            getResponsavel(idAbrigado)
          });

          $('#formResponsavel').on('submit', function(e){
            e.preventDefault();
            $.post('/admin/responsavel/criarNovoFimAjax',
              $('#formResponsavel').serialize(), function(data){
                console.log(data)
              }
            )
            $("#modalGeral").modal("hide");
        
          });

        })
      });
    }


 });

 $(".addPulseira").on("click", function(){
  let pessoaId = $(this).data('id');
  exibirTelaComplemento(`<h2>As pulseiras já foram entregues </h2>
  <p>Por favor, sempre solicite a devolução da pulseira antiga, mesmo que mesma esteje danificada</p>
  <label>Informe o motivo da troca de pulseira
  <textarea name="observacao" class="form-control">Primeira entrega</textarea>

  </label>
`, pessoaId, 15, function(pessoaId){
  let val = parseInt($("#qtdPul"+pessoaId).html())+1
  $("#qtdPul"+pessoaId).html(val)
})
 });

  $("#tabelaAbrigados .selectEstado").on("change", function () {
    const select = $(this)[0];
    const id = $(this).data('id')
    //pessoas[id].situacao = $(this).val()
    //console.table(pessoas[id]);
    //console.log(pessoas);
    //console.log(pessoas[id]);
    // setTimeout(() => {
    //   console.log(pessoas);
    // })
    var current = $(this).val();
    const selectedOption = select.options[select.selectedIndex];
    $(this).removeClass().addClass('selectEstado ' + selectedOption.className);



    if (current == 1) {
      //saida temporária
      exibirTelaComplemento(`
          <h2>Observações para saída temporária</h2>

          <textarea name="observacao" class="form-control"></textarea>
      `, id, 1);     
  
    }

    if (current == 12) {
      //Banho
      exibirTelaComplemento(`
          <h2>Digite a ficha do banho</h2>

          <input name="observacao" type="text" class="form-control"/>
      `, id, 12);     
  
    }

    if (current == 3) {
      //Desocupou
      $("#modalGeral").modal("show");
        $("#modalGeral .modal-body").html(
          `<h2>Verifique </h2>
          <ol>
              <li> Recolha as pulseiras</li>
              <li> Solicite a um voluntário volante o recolhimento do colchão </li>
              <li> Solicite kits de doações (comida, roupas e produtos de limpeza) </li>
          </ol>
          `
        );
    }

    $.get(
      "/admin/abrigado/atualizarSituacao/" + $(this).data("id") + "/" + current,
      function (data) {
        // console.log(data)
      }
    );



    if (current == "null") {
      $(this).css("background-color", "gray");
    }

 

  });

  //Não funcionou vai entender
  $("#apenasAbrigados").on('change', function(){
    if ($("#apenasAbrigados").prop('checked')) {
      document.location = '/admin/abrigado/telaGeral/0'
    } else {
      document.location = '/admin/abrigado/telaGeral/0'
    }
  });

  



});


