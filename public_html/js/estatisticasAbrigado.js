

const idadesDiv = document.getElementsByClassName('idade')
console.log(idadesDiv);

const pessoas = Array.from(idadesDiv).map((div) => {
    const situacao = $(div).closest('tr').find('.situacao').text().replace(/\s/g, '');
    const sala = $(div).closest('tr').find('.sala').text().replace(/\s/g, '');
    const salaId = $(div).closest('tr').find('.sala span').attr('class')
    return { idade: div.innerText, situacao: situacao, sala: sala, salaId: salaId }
})


const adultos = pessoas.filter(pessoa => pessoa.idade >= 18 && pessoa.idade <= 59 && pessoa.situacao != 'DESOCUPOU' && pessoa.situacao != 'ÓBITO' && pessoa.situacao != 'MOVIDO' && pessoa.situacao != 'TESTE')
const idosos = pessoas.filter(pessoa => pessoa.idade >= 65 && pessoa.situacao != 'DESOCUPOU' && pessoa.situacao != 'ÓBITO' && pessoa.situacao != 'MOVIDO' && pessoa.situacao != 'TESTE')
const adolescentes = pessoas.filter(pessoa => pessoa.idade >= 12 && pessoa.idade <= 17 && pessoa.situacao != 'DESOCUPOU' && pessoa.situacao != 'ÓBITO' && pessoa.situacao != 'MOVIDO' && pessoa.situacao != 'TESTE')
const criancas = pessoas.filter(pessoa => pessoa.idade <= 11 && pessoa.situacao != 'DESOCUPOU' && pessoa.situacao != 'ÓBITO' && pessoa.situacao != 'MOVIDO' && pessoa.situacao != 'TESTE')
const semData = pessoas.filter(pessoa => pessoa.idade == '-' && pessoa.situacao != 'DESOCUPOU' && pessoa.situacao != 'ÓBITO' && pessoa.situacao != 'MOVIDO' && pessoa.situacao != 'TESTE')

$('#adultos').text(adultos.length)
$('#idosos').text(idosos.length)
$('#adolescentes').text(adolescentes.length)
$('#criancas').text(criancas.length)
$('#c0-2').text(criancas.filter((c) => c.idade >= 0 && c.idade <= 2).length)
$('#c3-7').text(criancas.filter((c) => c.idade >= 3 && c.idade <= 7).length)
$('#c8-12').text(criancas.filter((c) => c.idade >= 8 && c.idade <= 12).length)
$('#semData').text(semData.length)


const totalJaAbrigados = pessoas.length
console.log(totalJaAbrigados);

const totalAbrigados = pessoas.filter((pessoa) => pessoa.situacao != 'DESOCUPOU' && pessoa.situacao != 'ÓBITO' && pessoa.situacao != 'MOVIDO' && pessoa.situacao != 'TESTE' && pessoa.situacao != 'VERIFICAR').length



const totalDesocupacoes = totalJaAbrigados - totalAbrigados
console.log(totalDesocupacoes);


$('#totalAbrigados').text(totalAbrigados)
$('#totalJaAbrigados').text(totalJaAbrigados)
$('#totalDesocupacoes').text(totalDesocupacoes)

$(document).ready(() => {
    $.get('/admin/sala/salas', (response) => {
        const salas = JSON.parse(response)

        salas.forEach(sala => {
            let nomeSala = sala.bloco + sala.sala
            if (sala.bloco == 'NÃO INFORMADO') {
                nomeSala = 'NÃO INFORMADO'
            }
            $('#salas .salaDiv').append(`<div>${nomeSala}: <span id="sala${sala.id}">0</span></div>`)

        });
        salasQuantidade = {}
        pessoas.forEach((pessoa) => {
            if (pessoa.situacao != 'DESOCUPOU' && pessoa.situacao != 'ÓBITO' && pessoa.situacao != 'MOVIDO' && pessoa.situacao != 'TESTE') {

                if (salasQuantidade[pessoa.salaId]) {
                    salasQuantidade[pessoa.salaId]++
                }
                else {
                    salasQuantidade[pessoa.salaId] = 1
                }
            }
        })

        Object.keys(salasQuantidade).forEach((sala) => {

            $('#' + sala).text(salasQuantidade[sala])
        })



    })


    $.get('/admin/situacao/situacoes', (response) => {
        const situacoes = JSON.parse(response)
        situacoes.forEach(situacao => {
            $('#situacoes').append(`<div><p>${situacao.situacao}</p> <span id="situacao${situacao.id}"></span></div>`)
            const quantidade = $(`.situacao${situacao.id}`).length
            $(`#situacao${situacao.id}`).text(quantidade)
        });
    })
})