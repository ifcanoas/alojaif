<?php

/**
 * Arquivo que apresenta as configurações para o envio de e-mails .
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package
 */

$fakeMail = getenv('FAKE_EMAIL') ?? 'false';
define('FAKE_EMAIL', $fakeMail);

define('MAIL_SERVER', 'email-ssl.com.br');
define('MAIL_PORT', '465');

$mailUser = getenv('MAIL_USER') ?? 'contato@revisaoonline.com.br';
define('MAIL_USER', $mailUser);

$mailPass = getenv('MAIL_PASS') ?? '';
define('MAIL_PASS', $mailPass);

#confs padrões
define('MAIL_SECURE', 'ssl');
define('MAIL_MODE', 'SMTP');



