<?php
/**
 * Arquivo com as configurações do sistema nesse arquivo fica as constantes
 * responsáveis pelo funcionamento correto do sistema. Configurações de usuário
 * e senha de Banco de dados por padrão não ficam nesse arquivo.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
if(is_file(__DIR__ . '/env.php')){
    require __DIR__ . '/env.php';
}

$file = str_replace('\\','/',__FILE__);//Workarround windows
$base = str_replace('confs/config.php','', $file);


//Constante que define o caminho onde fica o diretorio dos módulos do sistema
define('ROOT', $base. 'app/');

//Constante para a definição se o sistema esta em produção ou teste
$debug = !getenv('PRODUCAO');
define('DEBUG', $debug);
define('DEVEL', true);

//Constante que define o caminho onde fica o framework servidor do Enyalius
$eny = getenv('ENY') ? $base . 'core/': $base . 'vendor/enyalius/core/';
define('CORE', $eny);

//Constante que define o caminho onde fica o framework servidor do Enyalius
define('PUBLIC_DIR', $base . 'public_html/');

//Constante que define onde ficará os templates do sistema
define('TEMPLATES', ROOT . 'view/templates/');

//Constante para o Framework Smarty utilizar como cache, e outros frameworks usarem
//para armazenar arquivos de cache.
define('CACHE', $base . 'z_data/');

//Constante para o diretorio de logs
define('LOGS', CACHE . 'logs');

//define a chave de critpografia
define('LOGIN_CHAVE', '56973d6a8dbf7cd58330565e28f2b24d  -'); #TODO 

define('FIREBASE_API_ACCESS_KEY', 'AAAAhJqWd28:APA91bH3Pm7efI2ENHRnCXERjGjqfUVbxI2vLt1R8ANf-Hol_fadMNSBvdtsBVc1q68g45yzxF0frAfzAbOtla-oL8ocDEu0J50pP-nRl7_jm3cL4swf-RXmzg0H37Ctu2khVRSqegN4');
define('GTAG', '255665335s');
define('CAPTCHA_SERVER', '6Le4TcAZAAAAAA_Uumpgf2chE4hYweQnAWg2D4P4');
define('CAPTCHA_CLIENT_API', '6Le4TcAZAAAAAAGkxAdN5wx5agtauBLGUYMnBsVw');

//Configurações de hora
date_default_timezone_set('America/Sao_Paulo');

//Configuração de formato
setlocale(LC_ALL, 'pt_BR.utf8');

$lang = getenv('LANG') ? getenv('LANG') : 'pt-br';
define('LANG', $lang);

//Confs de usuario
$CONF = [];

define('REDIRECT_LOGIN', '/login');

//Require ambiente
require __DIR__ . '/conf_bd.php';
require __DIR__ . '/conf_mail.php';

