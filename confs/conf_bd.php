<?php

/**
 * Arquivo que apresenta as configurações de banco de dados.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package
 */


//Constante que define o servidor
$dbServer = getenv('DB_SERVER') ? getenv('DB_SERVER') : 'db';
define('DB_SERVER', $dbServer);

//Constante que define a porta do banco de dados
define('DB_PORT', '5432');

//Constante que define o usuário do banco de dados
$dbUser = getenv('POSTGRES_USER') ? getenv('POSTGRES_USER') : 'user';
define('DB_USER', $dbUser);

//Constante que define o usuário do banco de dados
$dbPass = getenv('POSTGRES_PASSWORD') ? getenv('POSTGRES_PASSWORD') : 'pass';
define('DB_PASSWORD', $dbPass);

//Constante que define o usuário do banco de dados
$dbName = getenv('POSTGRES_DB') ? getenv('POSTGRES_DB') : 'db';
define('DB_NAME', $dbName);

define('DB_TYPE', 'pgsql');



