#!/bin/sh
#
# Marcio Bigolin, Abril de 2020

HOST=abrigoprovisorio1.hospedagemdesites.ws
USER=abrigoprovisorio1
   

echo "Entrando em modo de UPLOAD"

HOST="ftp.$HOST"
echo $HOST
PASS=$1

LCD="$PWD/$2/"
RCD="$2"

#rever o arquivo HTACCESS
EXCLUDE_DEFAULT=" -x \.*.md$ \
        -x \.*.log$ \
        -x \.*.ts$ \
        -x \.*.scss$ \
        -x \.*.lock$ \
        --exclude-glob enyalius \
        --exclude-glob LICENSE \
        --exclude-glob CHANGELOG \
        --exclude-glob package.json \
        --exclude-glob doc/ \
        --exclude-glob .git/ \
        --exclude-glob view/core_client/ \
        --exclude-glob .htaccess \
        --exclude-glob testes/ \
        --exclude-glob vendor/ \
        --exclude-glob public_html/vendor \
        --exclude-glob build/  "
 
EXCLUDE_DEFAULT2="       
        --exclude-glob offline \
        --exclude-glob enyalius \
        --exclude-glob *.log \
        --exclude-glob *.lock \
        --exclude-glob *.scss \
        --exclude-glob *.dist \
        --exclude-glob *.yml \
        --exclude-glob .gitmodules \
        --exclude-glob .project \
        --exclude-glob .vscode/ \
        --exclude-glob pgpass
        --exclude-glob tools/.git\
        --exclude-glob Profile \
        --exclude-glob gulpfile.js \
        --exclude-glob phpunit \
        --exclude-glob vendor/phpunit \
        --exclude-glob nbproject/ \
        --exclude-glob www/ \
        --exclude-glob package.json \
        --exclude-glob public_html/index.php \
        --exclude-glob .htaccess \
        --exclude-glob public_html/css/src/ \
        --exclude-glob public_html/media/ \
        --exclude-glob node_modules/ --exclude node_modules \
        --exclude-glob core_view/ \
        --exclude-glob composer.json \
        --exclude-glob doc/ \
        --exclude-glob z_data/ \
        --exclude-glob procfile \
        --exclude-glob .gitignore \
        --exclude-glob testes/ \
        --exclude vendor/ --exclude-glob vendor/ \
        --exclude-glob pg_dump \
        --exclude-glob CHANGELOG \
        --exclude-glob vendor/core/view/core_client/node_modules/ \
        --exclude-glob vendor/core/view/core_client/css/ \
        --exclude-glob vendor/core/view/core_client/media/ \
        --exclude-glob vendor/core/view/core_client/js/ \
        --exclude-glob confs/"

echo "PWD $PWD \n\n"
FTPURL="ftp://$USER:$PASS@$HOST"

#DELETE="--delete"

COMAND="set ftp:list-options -a;  
    set ssl:verify-certificate no; 
    set ftp:ssl-allow true; 
    set ftp:ssl-force false;  
    set ftp:ssl-protect-data false; 
    set ftp:ssl-protect-list false; 
    open '$FTPURL';
    lcd $LCD;
   # rm -rf $RCD;
    mkdir -p $RCD;
    cd $RCD;
    mirror --reverse --verbose --no-symlinks --parallel=10 -x \.*.sh$ $EXCLUDE_DEFAULT "

#Execução
lftp -c "$COMAND"