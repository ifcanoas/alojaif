<?php

$db = new Model();
echo 'Ativando debug ...' . PHP_EOL;
$db->DB()->debugOn();

$DBTestes = ['abrigo'];
if(in_array(DB_NAME, $DBTestes)){
    echo 'Contornando a Locaweb... ' . PHP_EOL . PHP_EOL;
    $db->DB()->exec(file_get_contents(__DIR__ . '/extras/drop_all.sql'));
    $drop = true;
}else{
    echo 'Removendo versões anteriores ... ' . PHP_EOL . PHP_EOL;
    $oldSchema = 'old_' . date('d_m_hi');
    try{
        $db->DB()->exec('ALTER SCHEMA public RENAME TO ' . $oldSchema);
    }catch(Exception $e){
        echo 'Não consegui renomear PUBLIC vamos ver se dá para continuar';
    }
    $drop = $db->DB()->exec('CREATE SCHEMA public;');
}

$install = file_get_contents(__DIR__ . '/install.sql');
if ($db->DB()->exec($install) !== false) {

    echo 'Carregando dados ...' . PHP_EOL;

    $install = file_get_contents(__DIR__ . '/data.sql');
    if ($db->DB()->exec($install) !== false) {
        
    }

}