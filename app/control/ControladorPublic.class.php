<?php

class ControladorPublic extends AbstractController{

    public function __construct()
    {
        $this->view = new VisualizadorPublic();
    }

    public function index()
    {
        $this->pessoas();
    }

    public function pessoas()
    {
        $this->view->setTitle("Lista de pessoas acolhidas no abrigo do IFRS");
        $abrigados = Abrigado::getAll('situacao_id <> 13', 'nome_completo asc');
        $this->view->attValue('abrigados', $abrigados);
        $this->view->attValue('situacaoFragment', new SituacaoFragment(Situacao::getMap()));
        $this->view->addTemplate('publico/lista');
        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaAbrigados');
        $dataTable->addConf("bPaginate", false);
        $this->view->addComponente($dataTable);
        $this->view->addCSS('situacoes');
        $this->view->addCSS('abrigados');
        
    }

    public function doacao()
    {
        $this->view->setTitle("Doações IFRS");
        $this->view->addTemplate('publico/doacao');
        $this->view->addCSS('doacao');
        
    }

    public function pets()
    {
        $this->view->setTitle("Pets IFRS");
        $this->view->addTemplate('publico/pets');
        $this->view->addCSS('pets');
        $dao = new PetDAO();
        $this->view->attValue('pets', $dao->getAllByCampus());
        $this->view->attValue('tipos', TipoPet::getMap());
        
    }

    private function geraMiniatura($path){
        $miniPath = str_replace('.jpg', '_mini.jpg', $path);
        
        if(!file_exists($miniPath)){
            new Redimensionador($path, $miniPath, 100, 100);
            list($largura, $altura, $tipo) = getimagesize($miniPath);

            if($largura > $altura){
                // Load
                $source = imagecreatefromjpeg($miniPath);
        
                // Rotate
                $rotate = imagerotate($source, -90, 0);
        
                // Output
                imagejpeg($rotate, $miniPath);
        
                // Free the memory
                imagedestroy($source);
                imagedestroy($rotate);
            }
        
        }
        
        header ('Content-type: image/jpg');
        readfile($miniPath);
     
        exit();
    }

    public function getMiniaturaPet($id){
        $pet = Pet::getOne($id);
        $path = $pet->getFoto();

        $this->geraMiniatura($path);
    
    }

    public function CSV( $chave, $tipo = 'abrigado')
    {
        
        if($chave == '6987if2131'){
            $data = '';
            if($tipo == 'abrigado'){
                $list = Abrigado::getList(false, 'nome_completo ASC');
                $sala = new SalaFragment();
                $situacao = new SituacaoFragment();
                $data .= '"id","Nome do abrigado","idade","Bairro","Data Saída","Sala","Situação"'; #"número de cachorros","Familia"' . PHP_EOL;
                foreach($list as $line){
                    $data .= '"' . $line->getId() . '",';
                    $data .= '"' . $line->getNomeCompleto() . '",';
                    $data .= '"' . $line->getIdade() . '",';
                    $data .= '"' . $line->getBairro() . '",';
                    $data .= '"' . $line->getDataSaida() . '",';
                    $data .= '"' . $sala->getLabel($line->getSalaId()) . '",';
                    $data .= '"' . $situacao->getLabel($line->getSituacaoId() ). '"';



                    $data .= PHP_EOL;
                }
            }
            $this->response($data);
        }else{
            $this->index();
        }

    }

    public function voluntario()
    {
        $this->view->setTitle('');
    }

    public function paginaNaoEncontrada()
    {
        $this->index();
    }
}