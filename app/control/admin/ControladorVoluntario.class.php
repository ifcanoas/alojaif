<?php

/**
 * Classe controladora referente ao objeto Voluntario para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 10-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorVoluntario extends ControladorAdmin
 {

    /**
     * @var VoluntarioDAO
     */
    protected $model;



     /**
     * Construtor da classe Voluntario e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new VoluntarioDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

    public function telaGeral(){
        // $this->view->addTemplate('voluntarios/tela');
        // $this->view->setTitle('Voluntarios');
        // $this->view->attValue('voluntarios', Voluntario::getAll());
        // $this->view->attValue('tipos', TipoVoluntario::getSimpleMap('tipo'));
        // $this->view->addCSS('voluntarios');
        $this->tela();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Voluntario');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados('/admin//voluntario/tabela');
        $tabela->setTitulo('Voluntario');
        $tabela->addAcaoAdicionar( 
        '/admin//voluntario/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//voluntario/editar');
        $tabela->addAcaoDeletar( 
        '/admin//voluntario/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Nome completo', 'nome_completo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);
        
        $tabelaColuna = new TabelaColuna('Cpf', 'cpf');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Telefone', 'telefone');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Foto', 'foto');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo id', 'tipo_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Obs', 'obs');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Voluntario
     *
     * @param Voluntario $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Voluntario $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $voluntario = $obj == null ? new Voluntario() : $obj;

        $this->view->setTitle('Novo Voluntario');

        $this->view->attValue('voluntario', $voluntario);


        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm('/admin//voluntario/criarNovoFim' . $return);
        $this->view->addTemplate('forms/voluntario');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Voluntario $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Voluntario $obj = null) 
    {
        if($obj == null){
            $voluntario = $this->model->getById($id);
        }else{
            $voluntario = $obj;
        }

        $this->view->setTitle('Editar Voluntario');

        $this->view->attValue('voluntario', $voluntario);


        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm('/admin//voluntario/editarFim');
        $this->view->addTemplate('forms/voluntario');
        $this->view->endForm();

    }
    public function editarModal(int $id, Voluntario $obj = null) 
    {
        if($obj == null){
            $voluntario = $this->model->getById($id);
        }else{
            $voluntario = $obj;
        }

        $this->view->setTitle('Editar Voluntario');

        $this->view->attValue('voluntario', $voluntario);


        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm('/admin//voluntario/editarFim');
        $this->view->addTemplate('forms/voluntario');
        $this->view->endForm();

        $this->view->renderAjax();
    }


    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        dbd();
        $voluntario = new Voluntario();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();

            if(isset($_FILES["foto"]) && $_FILES["foto"]["tmp_name"]){

                $caminho_temporario = $_FILES["foto"]["tmp_name"];
                $dados["foto"] = $this->uploadFoto($caminho_temporario);;
            }

            if($voluntario->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($voluntario)){
                #$this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->redirect('/admin/voluntario/tela');
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Voluntario: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($voluntario);
    }

    /**
     * Controla a atualização dos objetos Voluntario na tabela 
     *
     */
    public function editarFim()
    {
        $this->redirectIfNoData('idVoluntario', BASE_URL . '/admin//voluntario/manter');
        $voluntario = new Voluntario();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $voluntario->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if(isset($_FILES["foto"]) && $_FILES["foto"]["tmp_name"]){
                $caminho_temporario = $_FILES["foto"]["tmp_name"];
                $dados["foto"] = $this->uploadFoto($caminho_temporario);;
            }
            if ($voluntario->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($voluntario)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->redirect('/admin/voluntario/tela');
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Voluntario: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $voluntario);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $voluntario = new Voluntario();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $voluntario->setId($id);
        try {
             if($this->model->delete($voluntario) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar Voluntario: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
        $consulta = $this->model->queryTable('tipo_voluntario', 'id, tipo');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'tipo');
        $this->view->attValue('listaTipos', $lista);
    }

    public function tela(){
        $this->view->setTitle('Tela de controle');
        $this->view->addTemplate('voluntarios/tela');
        //ds($condicao);
        $this->view->attValue('voluntarios', Voluntario::getList(false, 'nome_completo'));
        $this->view->attValue('tipos', TipoVoluntario::getSimpleMap('tipo'));

        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaVoluntario');
        $dataTable->addConf("bPaginate", false);
        $dataTable->addConf("order", []);

        $this->view->addComponente($dataTable);

        $this->view->setTitle('Voluntario');
        $this->view->addCSS('voluntario');
        $this->view->addJS('voluntario');
        $this->view->addJS('pluginAcentos');

        Componente::load('Modal');

        $modal = (new Modal('modalVoluntario'))->setTitulo('Voluntario');
        
        $this->view->addComponente($modal);
    }
    public function uploadFoto($caminho_temporario){
        $hash_md5 = md5_file($caminho_temporario);
        $diretorio_destino = CACHE . "uploads/";
        $caminho_destino = $diretorio_destino . $hash_md5 . ".jpg";
        ds($caminho_destino);
        move_uploaded_file($caminho_temporario, $caminho_destino);
        return $caminho_destino;
    }

    public function getFoto($id){
        $voluntario = Voluntario::getOne($id);
        $path = $voluntario->getFoto();

        header ('Content-type: image/png');
        readfile($path);
        exit();
    }
    public function getMiniatura($id){
        //return $this->getFoto($id);//Workarround para dormir
        $voluntario = Voluntario::getOne($id);
        $path = $voluntario->getFoto();

        $miniPath = str_replace('.jpg', '_mini.jpg', $path);
        
        if(!file_exists($miniPath)){
            new Redimensionador($path, $miniPath, 100, 100);
            list($largura, $altura, $tipo) = getimagesize($miniPath);

            if($largura > $altura){
                // Load
                $source = imagecreatefromjpeg($miniPath);
        
                // Rotate
                $rotate = imagerotate($source, -90, 0);
        
                // Output
                imagejpeg($rotate, $miniPath);
        
                // Free the memory
                imagedestroy($source);
                imagedestroy($rotate);
            }
        
        }
        header ('Content-type: image/jpg');
        readfile($miniPath);
     
        exit();
    }
}
