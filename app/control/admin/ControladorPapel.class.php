<?php

/**
 * Classe controladora referente ao objeto Papel para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 22-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorPapel extends ControladorAdmin
 {

    /**
     * @var PapelDAO
     */
    protected $model;

     /**
     * Construtor da classe Papel e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new PapelDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Papel');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//papel/tabela');
        $tabela->setTitulo('Papel');
        $tabela->addAcaoAdicionar( 
        '/admin//papel/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//papel/editar');
        $tabela->addAcaoDeletar( 
        '/admin//papel/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Usuário', 'usuario');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Senha', 'senha');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Nome papel', 'nome_papel');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Rota base', 'rota_base');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Papel
     *
     * @param Papel $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Papel $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $papel = $obj == null ? new Papel() : $obj;

        $this->view->setTitle('Novo Papel');

        $this->view->attValue('papel', $papel);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//papel/criarNovoFim' . $return);
        $this->view->addTemplate('forms/papel');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Papel $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Papel $obj = null) 
    {
        if($obj == null){
            $papel = $this->model->getById($id);
        }else{
            $papel = $obj;
        }

        $this->view->setTitle('Editar Papel');

        $this->view->attValue('papel', $papel);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//papel/editarFim');
        $this->view->addTemplate('forms/papel');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $papel = new Papel();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($papel->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($papel)){
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Papel: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($papel);
    }

    /**
     * Controla a atualização dos objetos Papel na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('id', '/admin//papel/manter');
        $papel = new Papel();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $papel->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($papel->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($papel)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Papel: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $papel);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $papel = new Papel();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $papel->setId($id);
        try {
             if($this->model->delete($papel) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar Papel: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
    }

}
