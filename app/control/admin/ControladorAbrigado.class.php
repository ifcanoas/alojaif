<?php

use SebastianBergmann\Type\FalseType;

/**
 * Classe controladora referente ao objeto Abrigado para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 11-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorAbrigado extends ControladorAdmin
 {

    /**
     * @var AbrigadoDAO
     */
    protected $model;

    private $statusNaoAbrigado = '3,7,8,10';

     /**
     * Construtor da classe Abrigado e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new AbrigadoDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->telaGeral(0);
    }

    public function baixaBanho(int $id){
        $abrigado = Abrigado::getOne($id);
        $registro = new Registro();
        $registro->setTipoRegistroId(12);
        $registro->setObservacao('Retorno da ficha:' . $abrigado->getUltimoRegistroBanho()->observacao);
        $registro->setAbrigadoId($id);
        $registro->save();
        $abrigado->setSituacaoId(2);
        $abrigado->save();
        $this->redirect('/admin/abrigado/banho');
    }

    public function refeicao()
    {
        $dataAtual = date("d/m/Y");
        $this->view->setTitle("Registro de refeição de $dataAtual");
        $this->view->addTemplate("refeicao/tela");
        $this->view->attValue('dataAtualVisual', $dataAtual);
        $condicao = 'situacao_id NOT IN (' . $this->statusNaoAbrigado . ')';
        $abrigados = Abrigado::getAll($condicao, 'nome_completo ASC');
        $this->view->attValue('abrigados', $abrigados);
        $registros = Registro::getAll();

        $dataAtual = date("Y-m-d");
        $registrosAlmoco = [];
        $registrosJanta = [];
        $quantidadeAlmoco = 0;
        $quantidadeJanta = 0;
        foreach ($registros as $registro) {
            $dataRegistro = substr($registro->getDataRegistro(), 0, 10);
            if ($dataRegistro === $dataAtual) {
                if ($registro->getTipoRegistroId() === 3) {
                    array_push($registrosAlmoco, $registro);
                    $quantidadeAlmoco++;
                }
                if ($registro->getTipoRegistroId() === 4) {
                    array_push($registrosJanta, $registro);
                    $quantidadeJanta++;
                }
            }
        }

        $this->view->attValue('dataAtual', $dataAtual);
        $this->view->attValue('registrosAlmoco', $registrosAlmoco);
        $this->view->attValue('registrosJanta', $registrosJanta);
        $this->view->attValue('quantidadeAlmoco', $quantidadeAlmoco);
        $this->view->attValue('quantidadeJanta', $quantidadeJanta);

        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaAbrigados');
        $dataTable->addConf("bPaginate", false);
        $this->view->addComponente($dataTable);

        $this->view->addCSS('abrigados');
        $this->view->addJS('refeicao');
        $this->view->addJS('pluginAcentos');
    }

    public function estatisticasRefeicao() {
        $this->view->addTemplate('refeicao/estatisticasRefeicao');
        $this->view->addCSS('estatisticasRefeicao');

        $model = new RegistroDAO();
        $dias = $model->getEstatisticasRefeicao();
        $this->view->attValue('dias', $dias);        
    }

    private function telaBase(){
        $this->view->setTitle('Tela de controle');
        $this->view->addTemplate('abrigados/tela_base');
        $this->view->attValue('abrigados', Abrigado::getAll('situacao_id NOT IN (' . $this->statusNaoAbrigado . ')', 'nome_completo ASC'));

        $this->view->attValue('situacoes', Situacao::getAll());
        $this->view->attValue('salas', Sala::getAll());
        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaAbrigados');
        $dataTable->addConf("bPaginate", false);
        $this->view->addComponente($dataTable);

        $this->view->addCSS('abrigados');

        $this->view->addJS('pluginAcentos');
        $this->view->addJS('abrigado_base');

        Componente::load('Modal');

        $modal = (new Modal('modalAbrigado'))->setTitulo('Abrigado');
        
        $this->view->addComponente($modal);

    }

    public function enfermaria(){
        $this->telaBase();

        $this->view->attValue('ACOES', 'upa/enfermaria_btn.tpl');
        $this->view->attValue('OBSERVACOES', 'upa/observacoes_enfermaria.tpl');

        $consulta = $this->model->queryTable('tipo_registro', 'id, tipo', 'sys = false');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'tipo');

        $this->view->attValue('tiposRegistro', $lista);
        $this->view->attValueJS('tiposRegistro', $lista);
    }

    public function higiene(){
        $this->telaBase();

        $this->view->attValue('ACOES', 'higiene/acoes.tpl');
        $this->view->attValue('OBSERVACOES', 'higiene/relatorio.tpl');

        $consulta = $this->model->queryTable('tipo_registro', 'id, tipo', 'sys = false');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'tipo');

        $this->view->attValue('tiposRegistro', $lista);
        $this->view->attValueJS('tiposRegistro', $lista);

        $this->view->addJS('higiene');

        $model =  new RegistroDAO();
        $this->view->attValue('registros', $model->getAllByCategoria('tipo_registro_id IN (7,6)', 'data_registro DESC'));
    }

    public function registroMedico(){
        $this->telaBase();
    }

    public function roupas(){
        $this->telaBase();

        $tiposRegistro = TipoRegistro::getList('id >= 25 AND id<=41 ', 'id', false, 'principal');
        $this->view->attValue('tiposDeRegistro', $tiposRegistro);
        $this->view->attValueJS('tiposRegistro', $tiposRegistro);

        

        $model =  new RegistroDAO();
        $this->view->attValue('registros', $model->getAllByCategoriaEAbrigado('tipo_registro_id >= 25 AND tipo_registro_id<=41 ', 'data_registro DESC'));

        $this->view->attValue('ACOES', 'roupas/acoes.tpl');
        $this->view->attValue('OBSERVACOES', 'roupas/relatorio.tpl');

        $this->view->addJS('roupas');
    }

    public function banhoAdd(){
        $this->view->addJS('banho_add');
        $this->telaBase();

        $this->view->attValue('ACOES', 'higiene/banho.tpl');


    }


    public function telaGeral($mostrarDesocupou = true)
    {
        $this->view->setTitle('Tela de controle');
        $this->view->addTemplate('abrigados/tela');
        $condicao = $mostrarDesocupou === true ? '': 'situacao_id NOT IN (' . $this->statusNaoAbrigado . ')';
        //ds($condicao);
        $this->view->attValue('apenasAbrigados', !$mostrarDesocupou);
        $this->view->attValue('abrigados', Abrigado::getAll($condicao, 'nome_completo ASC'));
        $this->view->attValue('situacoes', Situacao::getAll());
        $this->view->attValue('salas', Sala::getAll());
        $this->view->attValue('situacaoFragment', new SituacaoFragment(Situacao::getMap()));
        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaAbrigados');
        $dataTable->addConf("bPaginate", false);
        $dataTable->addConf("dom", '<"search"f>i'); 
        $dataTable->addConf("order", []);

        $this->view->addComponente($dataTable);

        $this->view->setTitle('Abrigado');
        $this->view->addCSS('abrigados');
        $this->view->addCSS('situacoes');
        $this->view->addJS('abrigado_base');

        $this->view->addJS('abrigados');
        $this->view->addJS('pluginAcentos');

        Componente::load('Modal');

        $modal = (new Modal('modalAbrigado'))->setTitulo('Abrigado');
        
        $this->view->addComponente($modal);

        $consulta = $this->model->queryTable('tipo_registro', 'id, tipo', 'sys = false');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'tipo');

        $this->view->attValueJS('tiposRegistro', $lista);


        $consulta = $this->model->queryTable('tipo_responsavel', 'id, tipo');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'tipo');
        $this->view->attValueJS('tiposResponsaveis', $lista);
        $this->view->attValue('registrosPulseira', Registro::getList('tipo_registro_id = 15'));
    }

    public function load(int $id){
        try{

            return $this->response(Abrigado::getOne($id));
        }catch(Exception $e){
            return $this->response(['erro'=>'erro'],500);
        }
    }

    public function banho(){
        $this->view->attValue('abrigados', Abrigado::getAll('situacao_id = 12', 'nome_completo ASC'));
        $this->view->addTemplate('abrigados/banho');
        $this->view->addJS('banho');
        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaAbrigados');
        $dataTable->addConf("bPaginate", false);
        $dataTable->addConf("order", []);

        $this->view->addComponente($dataTable);
    }
    
    public function relatorioSala()
    {
        $this->view->setTitle('Relatorio sala');

    }

    public function tudo($mostrarDesocupou = true){
        $this->view->setTitle('Tela com estatística');
        $this->view->addTemplate('abrigados/telaTudo');
        $condicao = $mostrarDesocupou === true ? '': 'situacao_id NOT IN (' . $this->statusNaoAbrigado . ')';
        //ds($condicao);
        $this->view->attValue('apenasAbrigados', !$mostrarDesocupou);
        $this->view->attValue('abrigados', Abrigado::getList($condicao, 'nome_completo ASC'));
        $this->view->attValue('situacoes', Situacao::getAll());
        $this->view->attValue('salas', Sala::getAll());
        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaAbrigados');
        $dataTable->allExport();
        $dataTable->addConf("bPaginate", false);
        $dataTable->addConf("order", []);

        $this->view->addComponente($dataTable);

        $this->view->setTitle('Abrigado');
        $this->view->addCSS('abrigados');
        $this->view->addJS('abrigado_base');
        $this->view->addJS('abrigados');
        $this->view->addJS('pluginAcentos');

    }
    

    /**
     * tela principal para o administrador
     *
     * @return void
     */
    public function estatisticas(){
        $this->view->addTemplate('abrigados/estatistica');
        $this->view->addCSS('abrigados');
        $this->view->addCSS('estatisticas');
        $this->view->addJS('estatisticasAbrigado');
        $this->view->attValue('abrigados', Abrigado::getList(false, 'nome_completo ASC'));
        $this->view->attValue('situacoes', Situacao::getAll());
        $this->view->attValue('salas', Sala::getAll());
        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaAbrigados');
        $dataTable->addConf("bPaginate", false);
        $dataTable->addConf("order", []);

        $this->view->addComponente($dataTable);

        $model = new RegistroDAO();
        $dias = $model->getEstatisticasRefeicao();
        $this->view->attValue('dias', $dias);   
    }

    public function enviarFoto(int $id)
    {
        $this->view->setRenderizado();
        if(isset($_FILES["foto"]) && $_FILES["foto"]["tmp_name"]){

            $caminho_temporario = $_FILES["foto"]["tmp_name"];
            $dados["foto"] = $this->uploadFoto($caminho_temporario);
            $abrigado = new Abrigado();
            $abrigado->setId($id);
            $abrigado->setFoto($dados["foto"]);
            $abrigado->save();
            $this->response('true', 200);
        }
        else {
            $this->response('false', 200);
        }
    }

    public function tirarFoto(){
        $this->view->setTitle('Tirar foto abrigado');
        $this->view->addTemplate('abrigados/tirarFoto');
        $this->view->attValue('abrigados', Abrigado::getList('situacao_id NOT IN (' . $this->statusNaoAbrigado . ') AND foto IS NULL', 'nome_completo ASC'));
        $this->view->addCSS('abrigadoFoto');
        $this->view->addCSS('abrigados');
        $this->view->addJS('fotosAbrigados');

        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaAbrigados');
        $dataTable->addConf("bPaginate", false);
        $dataTable->addConf("order", []);

        $this->view->addComponente($dataTable);

    }

    public function atualizarSituacao(int $idAbrigado, int $idSituacao){
        $abrigado = Abrigado::getOne($idAbrigado);
        $abrigado->setSituacaoId($idSituacao);
        if($idSituacao == 3){
            $abrigado->setDataSaida(date("Y-m-d h:i:s"));
        }
        $abrigado->save();
    }


     /**
     * Controla a inserção de um novo registro em Abrigado
     *
     * @param Abrigado $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Abrigado $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $abrigado = $obj == null ? new Abrigado() : $obj;

        $this->view->setTitle('Novo Abrigado');

        $this->view->attValue('abrigado', $abrigado);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm( '/admin//abrigado/criarNovoFim' . $return);
        $this->view->addTemplate('forms/abrigado');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Abrigado $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Abrigado $obj = null) 
    {
        if($obj == null){
            $abrigado = Abrigado::getOne($id);
        }else{
            $abrigado = $obj;
        }

        $this->view->setTitle('Editar Abrigado');


        $this->view->attValue('abrigado', $abrigado);

        $registro = Registro::getList('abrigado_id = '.$abrigado->getId(), 'data_registro DESC');
        $this->view->attValue('registros', $registro);

        $pets = Pet::getAll('abrigado_id = '.$abrigado->getId());
        $this->view->attValue('pets', $pets);

        $model = new AbrigadoDAO();
        $responsaveis = $model->getResponsaveis($abrigado->getId());
        $this->view->attValue('responsaveis', $responsaveis);

        $consulta = $this->model->queryTable('tipo_responsavel', 'id, tipo');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'tipo');
        $this->view->attValue('tiposResponsaveis', $lista);

        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaRegistro');
        $this->view->addComponente($dataTable);
        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//abrigado/editarFim');
        $this->view->addTemplate('forms/abrigado');
        $this->view->endForm();
        
        $this->view->renderAjax();
    }


    public function getFotoModal($id)
    {
        $this->view->attValue('id', $id);
        $this->view->addTemplate('abrigados/foto_modal'); 
        $this->view->renderAjax();
    }

    //Rever esse cara que está igual ao editar. 
    public function ver(int $id, $ajax = false) 
    {
        $abrigado = Abrigado::getOne($id);

        $this->view->setTitle('Ver Abrigado');


        $this->view->attValue('abrigado', $abrigado);

        $registro = Registro::getList('abrigado_id = '.$abrigado->getId(), 'data_registro DESC');
        $this->view->attValue('registros', $registro);

        $pets = Pet::getAll('abrigado_id = '.$abrigado->getId());
        $this->view->attValue('pets', $pets);

        $model = new AbrigadoDAO();
        $responsaveis = $model->getResponsaveis($abrigado->getId());
        $this->view->attValue('responsaveis', $responsaveis);

        $consulta = $this->model->queryTable('tipo_responsavel', 'id, tipo');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'tipo');
        $this->view->attValue('tiposResponsaveis', $lista);

        Componente::load('DataTable');
        $dataTable = new DataTable('#tabelaRegistro');
        $this->view->addComponente($dataTable);
        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->addTemplate('abrigados/ver');
        
        if($ajax){
            $this->view->renderAjax();
        }
    }


    public function nucleoFamiliar(){
        $this->view->setTitle('Núcleo Familiar');
        $this->view->addTemplate('abrigados/familia');

        $this->view->attValue('salas', Sala::getMap());

        $dao =  new AbrigadoDAO();

        $this->view->attValue('familias', $dao->getFamilias());
        $this->view->attValue('semFamilia', $dao->getAbrigadosSemFamilia());
    }



    public function nucleoFamiliarHigiene(){
        $this->view->setTitle('Núcleo Familiar Higiene/Limpeza');
        $this->view->addTemplate('higiene/familia_higiene');

        $this->view->attValue('salas', Sala::getMap());

        $model =  new RegistroDAO();
        $this->view->attValue('registros', $model->getAllByCategoria('tipo_registro_id IN (7,6)', 'data_registro DESC'));


        $dao =  new AbrigadoDAO();

        $this->view->attValue('familias', $dao->getFamilias());
        $this->view->attValue('semFamilia', $dao->getAbrigadosSemFamilia());

        $model =  new RegistroDAO();
        $this->view->attValue('registros', $model->getAllByCategoria('tipo_registro_id IN (7,6)', 'data_registro DESC'));
        
    }

   

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $abrigado = new Abrigado();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();

            if(isset($_FILES["foto"]) && $_FILES["foto"]["tmp_name"]){

                $caminho_temporario = $_FILES["foto"]["tmp_name"];
                $dados["foto"] = $this->uploadFoto($caminho_temporario);;
            }

            if($abrigado->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($abrigado->save(true)){
                #$this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->redirect("/admin/abrigado/telaGeral/0");
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Abrigado: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($abrigado);
    }

    /**
     * Controla a atualização dos objetos Abrigado na tabela 
     *
     */
    public function editarFim()
    {
        $this->redirectIfNoData('idAbrigado', BASE_URL . '/admin//abrigado/manter');
        $abrigado = new Abrigado();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $abrigado->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if(isset($_FILES["foto"]) && $_FILES["foto"]["tmp_name"]){
                $caminho_temporario = $_FILES["foto"]["tmp_name"];
                $dados["foto"] = $this->uploadFoto($caminho_temporario);;
            }
            if ($abrigado->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($abrigado)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->redirect("/admin/abrigado/telaGeral/0");
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Abrigado: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $abrigado);
    }



    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim($id)
    {
        if($_SESSION['logado'] == 'admin'){            
        
            $abrigado = new Abrigado();
            $abrigado->setId($id);
            try {
                if($this->model->delete($abrigado) !== false){
                    $this->view->addMensagemSucesso('Dado removido com sucesso!');
                }else{
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de deletar Abrigado: ' . json_encode($this->model->DB()->getLogErrors()));
                }
            }catch (IOException $e){ 
                $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
                $erro .= 'sistema e solucionado o mais breve possível.';
                $this->view->addMensagemErro($erro);
            }
        }
        $this->redirect('/admin/abrigado/estatisticas');
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
        $consulta = $this->model->queryTable('situacao', 'id, situacao');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'situacao');
        $this->view->attValue('listaSituacao', $lista);

        //ATENÇÃO FICOU FEIO NO FRONT COM UM JSON, MAS ACHEI MELHOR ASSIM DO SÓ MOSTANDO A SALA - tive que adaptar para mandar para o front a sala e o bloco

        //$consulta = $this->model->queryTable('sala', 'id, sala');
        //$lista = $this->model->getMapaSimplesDados($consulta, 'id', 'sala');
        //$this->view->attValue('listaSala', $lista);
        $lista = Sala::getAll();
        $salas = [];
        foreach($lista as $sala){
            $salas[$sala->getId()] = $sala->getBloco() . $sala->getSala(); 
        }
        $this->view->attValue('listaSala', $salas);
    }

    public function situacoesAbrigados() {
        $this->view->attValue('abrigados', Abrigado::getAll(false, 'nome_completo ASC'));   
    }

    public function getFoto($id){
        $abrigado = Abrigado::getOne($id);
        $path = $abrigado->getFoto();

        header ('Content-type: image/png');
        readfile($path);
        exit();
    }

    public function getMiniatura($id){
        $abrigado = Abrigado::getOne($id);
        $path = $abrigado->getFoto();

        $this->geraMiniatura($path);
    }

 
    //relatorio pet
    //select tipo, count(*) from pet inner join abrigado a on a.id = abrigado_id WHERE situacao_id <> 3 GROUP BY tipo



}
