<?php

/**
 * Classe controladora referente ao objeto Tipo_responsavel para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 17-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorTipoResponsavel extends ControladorAdmin
 {

    /**
     * @var TipoResponsavelDAO
     */
    protected $model;

     /**
     * Construtor da classe Tipo_responsavel e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new TipoResponsavelDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Tipo responsavel');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//tipoResponsavel/tabela');
        $tabela->setTitulo('Tipo responsavel');
        $tabela->addAcaoAdicionar( 
        '/admin//tipoResponsavel/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//tipoResponsavel/editar');
        $tabela->addAcaoDeletar( 
        '/admin//tipoResponsavel/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo', 'tipo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Tipo responsavel
     *
     * @param TipoResponsavel $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(TipoResponsavel $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $tipoResponsavel = $obj == null ? new TipoResponsavel() : $obj;

        $this->view->setTitle('Novo Tipo responsavel');

        $this->view->attValue('tipoResponsavel', $tipoResponsavel);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//tipoResponsavel/criarNovoFim' . $return);
        $this->view->addTemplate('forms/tipo_responsavel');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param TipoResponsavel $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, TipoResponsavel $obj = null) 
    {
        if($obj == null){
            $tipoResponsavel = $this->model->getById($id);
        }else{
            $tipoResponsavel = $obj;
        }

        $this->view->setTitle('Editar Tipo responsavel');

        $this->view->attValue('tipoResponsavel', $tipoResponsavel);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//tipoResponsavel/editarFim');
        $this->view->addTemplate('forms/tipo_responsavel');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $tipoResponsavel = new TipoResponsavel();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($tipoResponsavel->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($tipoResponsavel)){
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir TipoResponsavel: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($tipoResponsavel);
    }

    /**
     * Controla a atualização dos objetos TipoResponsavel na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idTipoResponsavel', BASE_URL . '/admin//tipoResponsavel/manter');
        $tipoResponsavel = new TipoResponsavel();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $tipoResponsavel->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($tipoResponsavel->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($tipoResponsavel)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar TipoResponsavel: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $tipoResponsavel);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $tipoResponsavel = new TipoResponsavel();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $tipoResponsavel->setId($id);
        try {
             if($this->model->delete($tipoResponsavel) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar TipoResponsavel: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
    }

}
