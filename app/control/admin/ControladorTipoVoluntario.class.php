<?php

/**
 * Classe controladora referente ao objeto Tipo_voluntario para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 27-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorTipoVoluntario extends ControladorAdmin
 {

    /**
     * @var TipoVoluntarioDAO
     */
    protected $model;

     /**
     * Construtor da classe Tipo_voluntario e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new TipoVoluntarioDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Tipo voluntario');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados(BASE_URL . '/admin//tipoVoluntario/tabela');
        $tabela->setTitulo('Tipo voluntario');
        $tabela->addAcaoAdicionar(BASE_URL . 
        '/admin//tipoVoluntario/criarNovo');
        $tabela->addAcaoEditar(BASE_URL . 
        '/admin//tipoVoluntario/editar');
        $tabela->addAcaoDeletar(BASE_URL . 
        '/admin//tipoVoluntario/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo', 'tipo');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Tipo voluntario
     *
     * @param TipoVoluntario $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(TipoVoluntario $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $tipoVoluntario = $obj == null ? new TipoVoluntario() : $obj;

        $this->view->setTitle('Novo Tipo voluntario');

        $this->view->attValue('tipoVoluntario', $tipoVoluntario);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//tipoVoluntario/criarNovoFim' . $return);
        $this->view->addTemplate('forms/tipo_voluntario');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param TipoVoluntario $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, TipoVoluntario $obj = null) 
    {
        if($obj == null){
            $tipoVoluntario = $this->model->getById($id);
        }else{
            $tipoVoluntario = $obj;
        }

        $this->view->setTitle('Editar Tipo voluntario');

        $this->view->attValue('tipoVoluntario', $tipoVoluntario);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm(BASE_URL . '/admin//tipoVoluntario/editarFim');
        $this->view->addTemplate('forms/tipo_voluntario');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $tipoVoluntario = new TipoVoluntario();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($tipoVoluntario->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($tipoVoluntario)){
                $this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir TipoVoluntario: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($tipoVoluntario);
    }

    /**
     * Controla a atualização dos objetos TipoVoluntario na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idTipoVoluntario', BASE_URL . '/admin//tipoVoluntario/manter');
        $tipoVoluntario = new TipoVoluntario();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $tipoVoluntario->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($tipoVoluntario->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($tipoVoluntario)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar TipoVoluntario: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $tipoVoluntario);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $tipoVoluntario = new TipoVoluntario();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $tipoVoluntario->setId($id);
        try {
             if($this->model->delete($tipoVoluntario) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar TipoVoluntario: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
    }

}
