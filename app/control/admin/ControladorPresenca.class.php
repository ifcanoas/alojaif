<?php

/**
 * Classe controladora referente ao objeto Presenca para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 11-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorPresenca extends ControladorAdmin
 {

    /**
     * @var PresencaDAO
     */
    protected $model;

     /**
     * Construtor da classe Presenca e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new PresencaDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Presenca');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados('/admin//presenca/tabela');
        $tabela->setTitulo('Presenca');
        $tabela->addAcaoAdicionar( 
        '/admin//presenca/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//presenca/editar');
        $tabela->addAcaoDeletar( 
        '/admin//presenca/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Voluntario id', 'voluntario_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Presenca id', 'presenca_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Data registro', 'data_registro');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('timestamp without time zone');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Entrada', 'entrada');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('timestamp without time zone');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Saida', 'saida');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('timestamp without time zone');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Presenca
     *
     * @param Presenca $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Presenca $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $presenca = $obj == null ? new Presenca() : $obj;

        $this->view->setTitle('Novo Presenca');

        $this->view->attValue('presenca', $presenca);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm('/admin//presenca/criarNovoFim' . $return);
        $this->view->addTemplate('forms/presenca');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Presenca $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Presenca $obj = null) 
    {
        if($obj == null){
            $presenca = $this->model->getById($id);
        }else{
            $presenca = $obj;
        }

        $this->view->setTitle('Editar Presenca');

        $this->view->attValue('presenca', $presenca);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm('/admin//presenca/editarFim');
        $this->view->addTemplate('forms/presenca');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $presenca = new Presenca();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($presenca->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($presenca)){
                #$this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Presenca: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($presenca);
    }

    /**
     * Controla a atualização dos objetos Presenca na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idPresenca', BASE_URL . '/admin//presenca/manter');
        $presenca = new Presenca();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $presenca->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($presenca->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($presenca)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Presenca: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $presenca);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $presenca = new Presenca();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $presenca->setId($id);
        try {
             if($this->model->delete($presenca) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar Presenca: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
        $consulta = $this->model->queryTable('voluntario', 'id, voluntario');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'voluntario');
        $this->view->attValue('listaVoluntario', $lista);

        $consulta = $this->model->queryTable('funcao', 'id, funcao');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'funcao');
        $this->view->attValue('listaFuncao', $lista);

    }

}
