<?php

/**
 * Classe controladora referente ao objeto Compra para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 25-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorCompra extends ControladorAdmin
 {

    /**
     * @var CompraDAO
     */
    protected $model;

     /**
     * Construtor da classe Compra e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new CompraDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Compra');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//compra/tabela');
        $tabela->setTitulo('Compra');
        $tabela->addAcaoAdicionar( 
        '/admin//compra/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//compra/editar');
        $tabela->addAcaoDeletar( 
        '/admin//compra/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Nota fiscal', 'nota_fiscal');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Data compra', 'data_compra');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('timestamp without time zone');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Valor', 'valor');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('double precision');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Quantidade', 'quantidade');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Item', 'item');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Observação', 'observacao');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Razao social beneficiado', 'razao_social_beneficiado');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Nome fantasia', 'nome_fantasia');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Compra
     *
     * @param Compra $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Compra $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $compra = $obj == null ? new Compra() : $obj;

        $this->view->setTitle('Novo Compra');

        $this->view->attValue('compra', $compra);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//compra/criarNovoFim' . $return);
        $this->view->addTemplate('forms/compra');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Compra $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Compra $obj = null) 
    {
        if($obj == null){
            $compra = $this->model->getById($id);
        }else{
            $compra = $obj;
        }

        $this->view->setTitle('Editar Compra');

        $this->view->attValue('compra', $compra);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//compra/editarFim');
        $this->view->addTemplate('forms/compra');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $compra = new Compra();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($compra->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($compra)){
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Compra: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($compra);
    }

    /**
     * Controla a atualização dos objetos Compra na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idCompra', BASE_URL . '/admin//compra/manter');
        $compra = new Compra();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $compra->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($compra->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($compra)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Compra: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $compra);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $compra = new Compra();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $compra->setId($id);
        try {
             if($this->model->delete($compra) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar Compra: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
    }

}
