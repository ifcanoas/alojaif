<?php

/**
 * Classe controladora referente ao objeto Tipo_pessoa para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 23-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorTipoPessoa extends ControladorAdmin
 {

    /**
     * @var TipoPessoaDAO
     */
    protected $model;

     /**
     * Construtor da classe Tipo_pessoa e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new TipoPessoaDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Tipo pessoa');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//tipoPessoa/tabela');
        $tabela->setTitulo('Tipo pessoa');
        $tabela->addAcaoAdicionar( 
        '/admin//tipoPessoa/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//tipoPessoa/editar');
        $tabela->addAcaoDeletar( 
        '/admin//tipoPessoa/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo pessoa', 'tipo_pessoa');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Cor', 'cor');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Tipo pessoa
     *
     * @param TipoPessoa $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(TipoPessoa $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $tipoPessoa = $obj == null ? new TipoPessoa() : $obj;

        $this->view->setTitle('Novo Tipo pessoa');

        $this->view->attValue('tipoPessoa', $tipoPessoa);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//tipoPessoa/criarNovoFim' . $return);
        $this->view->addTemplate('forms/tipo_pessoa');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param TipoPessoa $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, TipoPessoa $obj = null) 
    {
        if($obj == null){
            $tipoPessoa = $this->model->getById($id);
        }else{
            $tipoPessoa = $obj;
        }

        $this->view->setTitle('Editar Tipo pessoa');

        $this->view->attValue('tipoPessoa', $tipoPessoa);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//tipoPessoa/editarFim');
        $this->view->addTemplate('forms/tipo_pessoa');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $tipoPessoa = new TipoPessoa();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($tipoPessoa->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($tipoPessoa)){
                $this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir TipoPessoa: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($tipoPessoa);
    }

    /**
     * Controla a atualização dos objetos TipoPessoa na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idTipoPessoa', BASE_URL . '/admin//tipoPessoa/manter');
        $tipoPessoa = new TipoPessoa();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $tipoPessoa->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($tipoPessoa->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($tipoPessoa)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar TipoPessoa: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $tipoPessoa);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $tipoPessoa = new TipoPessoa();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $tipoPessoa->setId($id);
        try {
             if($this->model->delete($tipoPessoa) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar TipoPessoa: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
    }

}
