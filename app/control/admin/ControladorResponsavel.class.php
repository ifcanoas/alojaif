<?php

/**
 * Classe controladora referente ao objeto Responsavel para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 17-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorResponsavel extends ControladorAdmin
 {

    /**
     * @var ResponsavelDAO
     */
    protected $model;

     /**
     * Construtor da classe Responsavel e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new ResponsavelDAO();
    }


    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Responsavel');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//responsavel/tabela');
        $tabela->setTitulo('Responsavel');
        $tabela->addAcaoAdicionar( 
        '/admin//responsavel/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//responsavel/editar');
        $tabela->addAcaoDeletar( 
        '/admin//responsavel/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Abrigado id', 'abrigado_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Responsavel id', 'responsavel_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo responsavel id', 'tipo_responsavel_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Observação', 'observacao');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Responsavel
     *
     * @param Responsavel $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Responsavel $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $responsavel = $obj == null ? new Responsavel() : $obj;

        $this->view->setTitle('Novo Responsavel');

        $this->view->attValue('responsavel', $responsavel);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//responsavel/criarNovoFim' . $return);
        $this->view->addTemplate('forms/responsavel');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Responsavel $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Responsavel $obj = null) 
    {
        if($obj == null){
            $responsavel = $this->model->getById($id);
        }else{
            $responsavel = $obj;
        }

        $this->view->setTitle('Editar Responsavel');

        $this->view->attValue('responsavel', $responsavel);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//responsavel/editarFim');
        $this->view->addTemplate('forms/responsavel');
        $this->view->endForm();
    }



    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $responsavel = new Responsavel();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($responsavel->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($responsavel->save()){
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Responsavel: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($responsavel);
    }

        /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFimAjax()
     {
        $this->view->renderAjax();
        $responsavel = new Responsavel();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($responsavel->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($responsavel->save()){
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Responsavel: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($responsavel);
    }

    /**
     * Controla a atualização dos objetos Responsavel na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idResponsavel', BASE_URL . '/admin//responsavel/manter');
        $responsavel = new Responsavel();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $responsavel->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($responsavel->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($responsavel)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Responsavel: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $responsavel);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $responsavel = new Responsavel();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $responsavel->setId($id);
        try {
             if($this->model->delete($responsavel) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
                  $this->redirect('/admin/abrigado/telaGeral/0');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar Responsavel: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }        
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
        $consulta = $this->model->queryTable('abrigado', 'abrigado_id, abrigado');
        $lista = $this->model->getMapaSimplesDados($consulta, 'abrigado_id', 'abrigado');
        $this->view->attValue('listaAbrigado', $lista);

        $consulta = $this->model->queryTable('abrigado', 'responsavel_id, abrigado');
        $lista = $this->model->getMapaSimplesDados($consulta, 'responsavel_id', 'abrigado');
        $this->view->attValue('listaAbrigado', $lista);

        $consulta = $this->model->queryTable('tipo_responsavel', 'tipo_responsavel_id, tipo_responsavel');
        $lista = $this->model->getMapaSimplesDados($consulta, 'tipo_responsavel_id', 'tipo_responsavel');
        $this->view->attValue('listaTipoResponsavel', $lista);

    }

}
