<?php

/**
 * Classe controladora referente ao objeto Registro para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 11-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorRegistro extends ControladorAdmin
 {

    /**
     * @var RegistroDAO
     */
    protected $model;

     /**
     * Construtor da classe Registro e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new RegistroDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->criarNovo();
    }



    /**
     * Controla a inserção de um novo registro em Registro
     *
     * @param Registro $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(Registro $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $registro = $obj == null ? new Registro() : $obj;

        $this->view->setTitle('Novo Registro');

        $this->view->attValue('registro', $registro);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm('/admin//registro/criarNovoFim' . $return);
        $this->view->addTemplate('forms/registro');
        $this->view->endForm();
    }

    public function criarNovoAjax($id)
     {
        $this->view->setRenderizado();

        $registro =  new Registro();
        $registro->setAbrigadoId($id);
        $this->view->setTitle('Novo Registro');

        $this->view->attValue('registro', $registro);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm('/admin//registro/criarNovoFimAjax');
        $this->view->addTemplate('forms/registro');
        $this->view->endForm();
        $this->view->renderAjax();
    }

     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param Registro $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, Registro $obj = null) 
    {
        $this->view->setRenderizado();
        if($obj == null){
            $registro = $this->model->getById($id);
        }else{
            $registro = $obj;
        }

        $this->view->setTitle('Editar Registro');

        $this->view->attValue('registro', $registro);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm('/admin//registro/editarFim');
        $this->view->addTemplate('forms/registro');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $registro = new Registro();
        try {
            unset($_POST['abrigadoId']);
            $dados = ValidatorUtil::sanitizeForm();
            if($registro->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($registro->save()){
                #$this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir Registro: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($registro);
    }

    public function criarNovoFimAjax()
    {
     $this->view->renderAjax();
       $registro = new Registro();
       try {
           $dados = ValidatorUtil::sanitizeForm();
           if($registro->setArrayDados($dados) > 0){ 
               $this->view->addErros($GLOBALS['ERROS']);
           }else if($registro->save(true)){
               #$this->insertCascade();
               echo JSON::encode(['msg'=> 'Suscesso', 'id' => $registro->getID()]);
               return ;
           }else{
               $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
               _LOG::error('Falhou na hora de inserir Registro: ' . json_encode($this->model->DB()->getLogErrors()));
           }
       }catch (IOException $e){ 
            $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
            $erro .= 'sistema e solucionado o mais breve possível.';
            $this->view->addMensagemErro($erro);
       }
       echo JSON::encode(['msg'=> 'Erro']);
    }

    /**
     * Controla a atualização dos objetos Registro na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idRegistro', BASE_URL . '/admin//registro/manter');
        $registro = new Registro();
        $id = ValidatorUtil::variavelInt($_POST['abrigadoId']);
        $registro->setAbrigadoId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($registro->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($registro)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar Registro: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $registro);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim($id)
    {
        $registro = new Registro();
        $registro->setID($id);
        try {
            if ($this->model->delete($registro) !== false) {
                $this->redirect('/admin/abrigado/telaGeral/0');     
            } else {
                $this->view->addMensagemErro($this->model->getErro());
                _LOG::error('Falhou na hora de deletar Registro: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        } catch (IOException $e) {
            $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
            $erro .= 'sistema e solucionado o mais breve possível.';
            $this->view->addMensagemErro($erro);
        }
    }

      /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFimAjax($id)
    {
        $this->view->setRenderizado();
        $registro = new Registro();
        $registro->setID($id);
        try {
            if ($this->model->delete($registro) !== false) {
                return $this->response('Sucesso');
            } else {
                $this->view->addMensagemErro($this->model->getErro());
                _LOG::error('Falhou na hora de deletar Registro: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        } catch (IOException $e) {
            $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
            $erro .= 'sistema e solucionado o mais breve possível.';
            $this->response($erro, 500);
        }
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
        $consulta = $this->model->queryTable('abrigado', 'id, abrigado');
        $lista = $this->model->getMapaSimplesDados($consulta, 'id', 'abrigado');
        $this->view->attValue('listaAbrigado', $lista);

        $lista = TipoRegistro::getAll();
        $dadosFiltrados = [];
        foreach ($lista as $tipo) {
            $dadosFiltrados[$tipo->id] = $tipo->tipo;
        }
        $this->view->attValue('listaTipoRegistro', $dadosFiltrados);
        

    }

}
