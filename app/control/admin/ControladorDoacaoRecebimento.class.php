<?php

/**
 * Classe controladora referente ao objeto Doacao_recebimento para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 26-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorDoacaoRecebimento extends ControladorAdmin
 {

    /**
     * @var DoacaoRecebimentoDAO
     */
    protected $model;

     /**
     * Construtor da classe Doacao_recebimento e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new DoacaoRecebimentoDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

    public function pix(){
        $this->doacaoBase();

        $this->view->setTitle('PIX');

        $this->view->attValue('listaTipoRegistro', [1 =>'PIX']);

        $dao = new DoacaoRecebimentoDAO();
        $lista = $dao->getListaCompleta('tipo_doacao_id = 1');
        $this->view->attValue('doacoes', $lista);

        $this->view->attValue('ACAO_FORM', '/admin/doacaoRecebimento/adicionarAjax');
        $this->view->attValue('EXTRA', 'doacao/doacao_recebimento.tpl');
        

    }

    public function adicionarAjax(){
        dbd();
        $dados = ValidatorUtil::sanitizeForm();
        $pessoa = $this->inserePessoa($dados);

        $doacao = new DoacaoRecebimento();
        $doacao->setArrayDados($dados);
        $doacao->setPessoaId($pessoa->getID());
        $doacao->setTipoDoacaoId($dados['tipoRegistroId']);
        
        if($doacao->save()){
            return $this->response($doacao);
        }else{
            return $this->response('Problema ao inserir dados', 500);
        }

        
    }


     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Doação recebimento');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//doacaoRecebimento/tabela');
        $tabela->setTitulo('Doação recebimento');
        $tabela->addAcaoAdicionar( 
        '/admin//doacaoRecebimento/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//doacaoRecebimento/editar');
        $tabela->addAcaoDeletar( 
        '/admin//doacaoRecebimento/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Pessoa id', 'pessoa_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo doação id', 'tipo_doacao_id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Data recebimento', 'data_recebimento');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('timestamp without time zone');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Valor', 'valor');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('double precision');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Quantidade', 'quantidade');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Obs', 'obs');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Doação recebimento
     *
     * @param DoacaoRecebimento $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(DoacaoRecebimento $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $doacaoRecebimento = $obj == null ? new DoacaoRecebimento() : $obj;

        $this->view->setTitle('Novo Doação recebimento');

        $this->view->attValue('doacaoRecebimento', $doacaoRecebimento);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//doacaoRecebimento/criarNovoFim' . $return);
        $this->view->addTemplate('forms/doacao_recebimento');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param DoacaoRecebimento $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, DoacaoRecebimento $obj = null) 
    {
        if($obj == null){
            $doacaoRecebimento = $this->model->getById($id);
        }else{
            $doacaoRecebimento = $obj;
        }

        $this->view->setTitle('Editar Doação recebimento');

        $this->view->attValue('doacaoRecebimento', $doacaoRecebimento);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//doacaoRecebimento/editarFim');
        $this->view->addTemplate('forms/doacao_recebimento');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $doacaoRecebimento = new DoacaoRecebimento();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($doacaoRecebimento->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($doacaoRecebimento)){
                $this->insertCascade();
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir DoacaoRecebimento: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($doacaoRecebimento);
    }

    /**
     * Controla a atualização dos objetos DoacaoRecebimento na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idDoacaoRecebimento', BASE_URL . '/admin//doacaoRecebimento/manter');
        $doacaoRecebimento = new DoacaoRecebimento();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $doacaoRecebimento->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($doacaoRecebimento->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($doacaoRecebimento)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar DoacaoRecebimento: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $doacaoRecebimento);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $doacaoRecebimento = new DoacaoRecebimento();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $doacaoRecebimento->setId($id);
        try {
             if($this->model->delete($doacaoRecebimento) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar DoacaoRecebimento: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
        $consulta = $this->model->queryTable('pessoa', 'pessoa_id, pessoa');
        $lista = $this->model->getMapaSimplesDados($consulta, 'pessoa_id', 'pessoa');
        $this->view->attValue('listaPessoa', $lista);

        $consulta = $this->model->queryTable('tipo_doacao', 'tipo_doacao_id, tipo_doacao');
        $lista = $this->model->getMapaSimplesDados($consulta, 'tipo_doacao_id', 'tipo_doacao');
        $this->view->attValue('listaTipoDoacao', $lista);

    }

}
