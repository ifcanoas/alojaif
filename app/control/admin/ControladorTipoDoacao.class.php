<?php

/**
 * Classe controladora referente ao objeto Tipo_doacao para 
 * a manutenção dos dados no sistema 
 *
 * @package app.control
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 25-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class ControladorTipoDoacao extends ControladorAdmin
 {

    /**
     * @var TipoDoacaoDAO
     */
    protected $model;

     /**
     * Construtor da classe Tipo_doacao e  inicializa o modelo de dados 
     *
     */
    public function __construct() {
        parent::__construct();
        $this->model = new TipoDoacaoDAO();
    }

    /**
     * Redireciona para a página de manter dados  
     *
     */
    public function index()
    {
        $this->manter();
    }

     /**
      * Cria a tabela que serve de visualização para os dados.  
      * através dessa página pode se acessar as demans funcionalidades do CRUD.  
      *
      */
    public function manter()
    {
        $this->view->setTitle('Tipo doação');

        Componente::load('TabelaManterDados'); 
        $tabela = new TabelaManterDados();
        $tabela->setDados( '/admin//tipoDoacao/tabela');
        $tabela->setTitulo('Tipo doação');
        $tabela->addAcaoAdicionar( 
        '/admin//tipoDoacao/criarNovo');
        $tabela->addAcaoEditar( 
        '/admin//tipoDoacao/editar');
        $tabela->addAcaoDeletar( 
        '/admin//tipoDoacao/deletarFim');

         //Colunas da tabela
        $tabelaColuna = new TabelaColuna('Id', 'id');
        $tabelaColuna->setLargura(40);
        $tabelaColuna->setBuscaTipo('integer');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Tipo doação', 'tipo_doacao');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $tabelaColuna = new TabelaColuna('Cor', 'cor');
        $tabelaColuna->setLargura(60);
        $tabelaColuna->setBuscaTipo('character varying');
        $tabela->addColuna($tabelaColuna);

        $this->view->addComponente($tabela);
    }

    /**
     * Gera os dados json da tabela de manutenção dos dados 
     * e recebe os dados de consulta para a sua atualizacao 
     *
     */
    public function tabela()
     {
        $this->view->setRenderizado();
        Componente::load('TabelaConsulta');
        $tabela = new TabelaConsulta(ValidatorUtil::variavel($_POST['sidx']));
        $tabela->recebeDados($_POST);

        $dados = $this->model->getQueryTable($tabela);

        echo JSON::encode($dados);
    }



    /**
     * Controla a inserção de um novo registro em Tipo doação
     *
     * @param TipoDoacao $obj - Objeto DataTransfer com os dados da classe
     */
    public function criarNovo(TipoDoacao $obj = null)
     {
        $arg = $this->getARG(0);
        $return = !empty($arg) ? '/' . $arg : '';
        $tipoDoacao = $obj == null ? new TipoDoacao() : $obj;

        $this->view->setTitle('Novo Tipo doação');

        $this->view->attValue('tipoDoacao', $tipoDoacao);

        //Carrega os campos de seleção;
        $this->getSelects();
        $this->view->startForm(BASE_URL  . '/admin//tipoDoacao/criarNovoFim' . $return);
        $this->view->addTemplate('forms/tipo_doacao');
        $this->view->endForm();
    }


     /**
     * Edita os dados da tabela ou objeto em questão 
     *
     * @param TipoDoacao $obj - Objeto para carregar os formulários 
     */
    public function editar(int $id, TipoDoacao $obj = null) 
    {
        if($obj == null){
            $tipoDoacao = $this->model->getById($id);
        }else{
            $tipoDoacao = $obj;
        }

        $this->view->setTitle('Editar Tipo doação');

        $this->view->attValue('tipoDoacao', $tipoDoacao);

        //Carrega os campos de seleção;
        $this->getSelects();

        $this->view->startForm( '/admin//tipoDoacao/editarFim');
        $this->view->addTemplate('forms/tipo_doacao');
        $this->view->endForm();
    }

    /**
     * Controla a criação e inserção final de um registro no SGBD
     *
     */
    public function criarNovoFim()
     {
        $tipoDoacao = new TipoDoacao();
        try {
            unset($_POST['id']);
            $dados = ValidatorUtil::sanitizeForm();
            if($tipoDoacao->setArrayDados($dados) > 0){ 
                $this->view->addErros($GLOBALS['ERROS']);
            }else if($this->model->create($tipoDoacao)){
                $this->view->addMensagemSucesso('Dados inseridos com sucesso!');
                $this->manter();
                return ;
            }else{
                $this->view->addMensagemErro('Erro ao inserir seus dados tente novamente mais tarde.');
                _LOG::error('Falhou na hora de inserir TipoDoacao: ' . json_encode($this->model->DB()->getLogErrors()));
            }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->criarNovo($tipoDoacao);
    }

    /**
     * Controla a atualização dos objetos TipoDoacao na tabela 
     *
     */
    public function editarFim()
     {
        $this->redirectIfNoData('idTipoDoacao', BASE_URL . '/admin//tipoDoacao/manter');
        $tipoDoacao = new TipoDoacao();
        $id = ValidatorUtil::variavelInt($_POST['id']);
        $tipoDoacao->setId($id);
        try {
            $dados = ValidatorUtil::sanitizeForm();
            if ($tipoDoacao->setArrayDados($dados) > 0) { 
                $this->view->addErros($GLOBALS['ERROS']);
            }else{
                if ($this->model->update($tipoDoacao)) {
                    $this->view->addMensagemSucesso('Dados alterados com sucesso!');
                    $this->manter();
                    return ;
                } else {
                    $this->view->addMensagemErro($this->model->getErro());
                    _LOG::error('Falhou na hora de editar TipoDoacao: ' . json_encode($this->model->DB()->getLogErrors()));
                }
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->editar(0, $tipoDoacao);
    }

    /**
     * Controla a exclusão de dados na tabela final
     *
     */
    public function deletarFim()
    {
        $tipoDoacao = new TipoDoacao();
        $id = ValidatorUtil::variavelInt($GLOBALS['ARGS'][0]);
        $tipoDoacao->setId($id);
        try {
             if($this->model->delete($tipoDoacao) !== false){
                  $this->view->addMensagemSucesso('Dado removido com sucesso!');
             }else{
                  $this->view->addMensagemErro($this->model->getErro());
                  _LOG::error('Falhou na hora de deletar TipoDoacao: ' . json_encode($this->model->DB()->getLogErrors()));
             }
        }catch (IOException $e){ 
             $erro  = 'Ocorreu um erro pouco comum. O mesmo será cadastrado no ';
             $erro .= 'sistema e solucionado o mais breve possível.';
             $this->view->addMensagemErro($erro);
        }
        $this->manter();
    }


    /**
     * Cria os select 
     *
     */
    private function getSelects()
     {
    }

}
