<?php
/**
 * Classe para a transferencia de dados de DoacaoRecebimento entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 26-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class DoacaoRecebimento implements DTOInterface
{
    use core\model\DTOTrait;

    private $id;
    private $pessoaId;
    private $tipoDoacaoId;
    private $dataRecebimento;
    private $valor;
    private $quantidade = 1;
    private $obs;
    private $isValid;
    private $table;


    private $pessoa;
    private $tipoRegistro;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.doacao_recebimento')
    {
        $this->table = $table;
        $this->ignoreField( 'pessoa', 'tipoRegistro');

    }

    /**
     * Método que seta o valor da variável pessoaId
     *
     * @param int $pessoaId - Valor da variável pessoaId
     */
    public function setPessoaId($pessoaId)
    {
        if(empty($pessoaId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Pessoa id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($pessoaId) && is_int($pessoaId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Pessoa id não é um número inteiro válido!';
            return false;
        }
        $this->pessoaId = $pessoaId;
        return $this;
    }

    /**
     * Método que seta o valor da variável tipoDoacaoId
     *
     * @param int $tipoDoacaoId - Valor da variável tipoDoacaoId
     */
    public function setTipoDoacaoId($tipoDoacaoId)
    {
        if(empty($tipoDoacaoId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo doação id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($tipoDoacaoId) && is_int($tipoDoacaoId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo doação id não é um número inteiro válido!';
            return false;
        }
        $this->tipoDoacaoId = $tipoDoacaoId;
        return $this;
    }

    /**
     * Retorna o valor da variável dataRecebimento formatada 
     *
     * @param bool $comHora - opção para mostrar a hora ou não 
     * @param bool $extenso - opção para retornar a data por extenso ou não 
     * @return string - Valor da variável dataRecebimento formatada 
     */
    public function getDataRecebimentoFormatada($comHora = true, $extenso = true)
    {
        return $extenso ? DateUtil::formataDataExtenso($this->dataRecebimento, $comHora) : DateUtil::formataData($this->dataRecebimento, $comHora);
    }

    /**
     * Método que seta o valor da variável dataRecebimento
     *
     * @param string $dataRecebimento - Valor da variável dataRecebimento
     */
    public function setDataRecebimento($dataRecebimento)
    {
        if(empty($dataRecebimento)){
            $GLOBALS['ERROS'][] = 'O valor informado em Data recebimento não pode ser nulo!';
            return false;
        }
        $this->dataRecebimento = $dataRecebimento;
        return $this;
    }

    /**
     * Método que seta o valor da variável valor
     *
     * @param string $valor - Valor da variável valor
     */
    public function setValor($valor)
    {
        if(empty($valor)){
            $GLOBALS['ERROS'][] = 'O valor informado em Valor não pode ser nulo!';
            return false;
        }
        $valor = str_replace(',', '.', $valor);
        if(!is_numeric($valor)){
            $GLOBALS['ERROS'][] = 'O valor informado em  Valor não é um número válido!';
            return false;
        }
        $this->valor = $valor;
        return $this;
    }

    /**
     * Método que seta o valor da variável quantidade
     *
     * @param int $quantidade - Valor da variável quantidade
     */
    public function setQuantidade($quantidade)
    {
        if(empty($quantidade)){
            $GLOBALS['ERROS'][] = 'O valor informado em Quantidade não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($quantidade) && is_int($quantidade + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Quantidade não é um número inteiro válido!';
            return false;
        }
        $this->quantidade = $quantidade;
        return $this;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }
}
