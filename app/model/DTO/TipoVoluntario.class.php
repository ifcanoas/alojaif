<?php
/**
 * Classe para a transferencia de dados de TipoVoluntario entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 27-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class TipoVoluntario implements DTOInterface
{
    use core\model\DTOTrait;

    private $id;
    private $tipo;
    private $isValid;
    private $table;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.tipo_voluntario')
    {
        $this->table = $table;
    }

    /**
     * Método que seta o valor da variável tipo
     *
     * @param string $tipo - Valor da variável tipo
     */
    public function setTipo($tipo)
    {
        if(empty($tipo)){
            $GLOBALS['ERROS'][] = 'O valor informado em Tipo não pode ser nulo!';
            return false;
        }
        $this->tipo = $tipo;
        return $this;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }
}
