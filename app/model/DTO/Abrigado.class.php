<?php
/**
 * Classe para a transferencia de dados de Abrigado entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 11-05-2024(Gerado Automaticamente com GC - 2.0.0 29/08/2023)
 */

class Abrigado implements DTOInterface, JsonSerializable
{
    use core\model\DTOTrait;

    private $id;
    private $nomeCompleto;
    private $cpf;
    private $observacao;
    private $documento;
    private $cartaoSus;
    private $foto;
    private $dataNascimento;
    private $idadeOld;
    private $endereco;
    private $bairro;
    private $telefone;
    private $saidaTemporaria;
    private $dataSaida;
    private $situacaoId;
    private $salaId;
    private $isValid;
    private $table;

    /**
     * Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.abrigado')
    {
        $this->table = $table;
    }

    /**
     * Método que seta o valor da variável nomeCompleto
     *
     * @param string $nomeCompleto - Valor da variável nomeCompleto
     */
    public function setNomeCompleto($nomeCompleto)
    {
        if(empty($nomeCompleto)){
            $GLOBALS['ERROS'][] = 'O valor informado em Nome completo não pode ser nulo!';
            return false;
        }
        $this->nomeCompleto = $nomeCompleto;
        return $this;
    }

    /**
     * Retorna o valor da variável dataNascimento formatada 
     *
     * @param bool $comHora - opção para mostrar a hora ou não 
     * @param bool $extenso - opção para retornar a data por extenso ou não 
     * @return string - Valor da variável dataNascimento formatada 
     */
    public function getDataNascimentoFormatada($comHora = true, $extenso = true)
    {
        return $extenso ? DateUtil::formataDataExtenso($this->dataNascimento, $comHora) : DateUtil::formataData($this->dataNascimento, $comHora);
    }

    public function getUltimoRegistroBanho(){
        $registrosBanho = Registro::getList('abrigado_id = ' . $this->id .' AND tipo_registro_id = 12 ', 'data_registro DESC', '1' );
        //return implode(explode(' ', $registrosBanho[0]));
        return $registrosBanho[0];
    }

    public function getQuantidadePulseiras($registros)
    {
        return sizeof($this->getRegistros($registros));
    }

    public function getRegistro($registros) {
        foreach ($registros as $registro) {
            if ($registro->getAbrigadoId() === $this->id) {
                return $registro;
            }
        }
        return null;
    }

    public function getRegistros($registros) {
        $registrosLocal = [];
        foreach ($registros as $registro) {
            if ($registro->getAbrigadoId() === $this->id) {
                $registrosLocal[] = $registro;
            }
        }
        return $registrosLocal;
    }

    /**
     * Retorna o valor da variável idade 
     *
     * @return string - Valor da variável idade
     */
    public function getIdade()
    {
        $dataNasc = $this->getDataNascimentoFormatada(false, false);
        if ($dataNasc) {
            return $this->calcularIdade($dataNasc);
        }
        return '-';
    }

    /**
     * Método que seta o valor da variável idadeOld
     *
     * @param int $idadeOld - Valor da variável idadeOld
     */
    public function setIdadeOld($idadeOld)
    {
        if(!(is_numeric($idadeOld) && is_int($idadeOld + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Idade old não é um número inteiro válido!';
            return false;
        }
        $this->idadeOld = $idadeOld;
        return $this;
    }

    /**
     * Retorna o valor da variável dataSaida formatada 
     *
     * @param bool $comHora - opção para mostrar a hora ou não 
     * @param bool $extenso - opção para retornar a data por extenso ou não 
     * @return string - Valor da variável dataSaida formatada 
     */
    public function getDataSaidaFormatada($comHora = true, $extenso = true)
    {
        return $extenso ? DateUtil::formataDataExtenso($this->dataSaida, $comHora) : DateUtil::formataData($this->dataSaida, $comHora);
    }

    /**
     * Método que seta o valor da variável situacaoId
     *
     * @param int $situacaoId - Valor da variável situacaoId
     */
    public function setSituacaoId($situacaoId)
    {
        if(empty($situacaoId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Situação id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($situacaoId) && is_int($situacaoId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Situação id não é um número inteiro válido!';
            return false;
        }
        $this->situacaoId = $situacaoId;
        return $this;
    }

    /**
     * Método que seta o valor da variável salaId
     *
     * @param int $salaId - Valor da variável salaId
     */
    public function setSalaId($salaId)
    {
        if(empty($salaId)){
            $GLOBALS['ERROS'][] = 'O valor informado em Sala id não pode ser nulo!';
            return false;
        }
        if(!(is_numeric($salaId) && is_int($salaId + 0))){
            $GLOBALS['ERROS'][] = 'O valor informado em Sala id não é um número inteiro válido!';
            return false;
        }
        $this->salaId = $salaId;
        return $this;
    }

    /**
     * Retorna o valor de uma  chave primária
     *
     * @return misc - valor da chave primaria
     */
    public function getID(){
        return $this->id;
     }

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id = ' . $this->id;
     }


    private function calcularIdade($dataNascimento) {
        // Convertendo a data de nascimento em timestamp
        $dataNascimento = strtotime(str_replace('/', '-', $dataNascimento));
        
        // Obtendo a data atual
        $dataAtual = time();
        
        // Calculando a diferença em segundos entre as datas
        $diferenca = $dataAtual - $dataNascimento;
        
        // Convertendo a diferença em anos
        $idade = floor($diferenca / (60 * 60 * 24 * 365));
        
        return $idade;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->getId(),
            'nome' => $this->getNomeCompleto(),
        ];
    }
}
