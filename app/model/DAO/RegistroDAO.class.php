<?php

/**
 * Classe de modelo referente ao objeto Registro para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 12-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class RegistroDAO extends AbstractDAO
{
    public static $mostrarPrivados = false;

    /**
     * Construtor da classe RegistroDAO esse metodo  
     * instancia o Modelo padrão conectando o mesmo ao banco de dados
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Registro::table();
        $this->colunmID = 'id';
        $this->colunms = [
            'abrigado_id',
            'tipo_registro_id',
            'observacao',
            'tamanho',
            'quantidade',
            'data_registro',
            'valido'
        ];
    }

    public function getEstatisticasRefeicao()
    {
        $sql = 'SELECT registro.tipo_registro_id, registro.data_registro FROM registro';
        $query = $this->query($sql);

        $lista = [];

        $almoco = 0;
        $janta = 0;
        $datas = [];

        if ($query) {
            foreach ($query as $linhaBanco) {
                $registro = new Registro();

                $registro->setTipoRegistroId($linhaBanco['tipo_registro_id']);
                $registro->setDataRegistro($linhaBanco['data_registro']);


                $dataFormatada = $registro->getDataRegistroFormatada(false, false);

                if (!array_key_exists($dataFormatada, $datas)) {
                    $datas[$dataFormatada] = [];
                    $datas[$dataFormatada]['almoco'] = 0;
                    $datas[$dataFormatada]['janta'] = 0;
                }

                if ($registro->getTipoRegistroId() == '3') {
                    $datas[$dataFormatada]['almoco']++;
                }
                if ($registro->getTipoRegistroId() == '4') {
                    $datas[$dataFormatada]['janta']++;
                }
            }
        }

        return $datas;
    }


    public function getAllByCategoria($condicao = false, $order = false, $limit = false, $offset = false)
    {
        $data = $this->queryTable('registro', 'id as principal, *', $condicao, $order, $limit, $offset);
        $result = [];
        foreach ($data as $linha) {
            $registro = $this->setDados($linha);
            $result[$linha['tipo_registro_id']][] = $registro;
        }
        return $result;
    }

    public function getAllByCategoriaEAbrigado($condicao = false, $order = false, $limit = false, $offset = false)
    {
        $data = $this->queryTable('registro', 'id as principal, *', $condicao, $order, $limit, $offset);
        $result = [];
        foreach ($data as $linha) {
            $registro = $this->setDados($linha);
            $result[$linha['tipo_registro_id']][$linha['abrigado_id']][] = $registro;
        }
        return $result;
    }

    #Sobreescrever garantindo que não retorne os privados
    public function getList($condicao = false, $order = false, $limit = false, $key = false)
    {
        $where  = '';

        if (!RegistroDAO::$mostrarPrivados) {
            $where .= 'private <> true';
        }
        if (!RegistroDAO::$mostrarPrivados && $condicao) {
            $where .= ' AND ' . $condicao;
        } else {

            $where .= $condicao;
        }


        $query = $this->queryTable('registro_completo', $this->getColunmsString(), $where, $order, $limit);
        return $this->listByQuery($query, $key);
    }

    /**
     * Retorna um objeto setado Registro
     * com objetivo de servir as funções getTabela, getLista e getRegistro
     *
     * @param array $dados
     * @return objeto Registro
     */
    protected function setDados($dados)
    {
        $registro = new Registro();
        $registro->setId($dados['principal']);
        $registro->setAbrigadoId($dados['abrigado_id']);
        $registro->setTipoRegistroId($dados['tipo_registro_id']);
        $registro->setObservacao($dados['observacao']);
        $registro->setDataRegistro($dados['data_registro']);
        $registro->setQuantidade($dados['quantidade']);
        $registro->setTamanho($dados['tamanho']);
        $registro->setValido($dados['valido']);
        return $registro;
    }
}
