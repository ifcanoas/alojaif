<?php

/**
 * Classe de modelo referente ao objeto TipoPet para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 17-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class TipoPetDAO extends AbstractDAO 
{

    /**
    * Construtor da classe TipoPetDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  TipoPet::table();
        $this->colunmID = 'id';
        $this->colunms = [     'tipo'
                          ];
    }

    /**
     * Retorna um objeto setado TipoPet
     * com objetivo de servir as funções getTabela, getLista e getTipoPet
     *
     * @param array $dados
     * @return objeto TipoPet
     */
    protected function setDados($dados)
    {
        $tipoPet = new TipoPet();
        $tipoPet->setId($dados['principal']);
        $tipoPet->setTipo($dados['tipo']);
        return $tipoPet;
    }
}