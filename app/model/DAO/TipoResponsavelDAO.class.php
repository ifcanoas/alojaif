<?php

/**
 * Classe de modelo referente ao objeto TipoResponsavel para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 17-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class TipoResponsavelDAO extends AbstractDAO 
{

    /**
    * Construtor da classe TipoResponsavelDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  TipoResponsavel::table();
        $this->colunmID = 'id';
        $this->colunms = [     'tipo'
                          ];
    }

    /**
     * Retorna um objeto setado TipoResponsavel
     * com objetivo de servir as funções getTabela, getLista e getTipoResponsavel
     *
     * @param array $dados
     * @return objeto TipoResponsavel
     */
    protected function setDados($dados)
    {
        $tipoResponsavel = new TipoResponsavel();
        $tipoResponsavel->setId($dados['principal']);
        $tipoResponsavel->setTipo($dados['tipo']);
        return $tipoResponsavel;
    }
}