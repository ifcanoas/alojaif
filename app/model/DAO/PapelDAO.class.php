<?php

/**
 * Classe de modelo referente ao objeto Papel para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 22-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class PapelDAO extends AbstractDAO 
{

    /**
    * Construtor da classe PapelDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Papel::table();
        $this->colunmID = 'id';
        $this->colunms = [     'usuario',
                                'senha',
                                'nome_papel',
                                'rota_base'
                          ];
    }

    /**
     * Retorna um objeto setado Papel
     * com objetivo de servir as funções getTabela, getLista e getPapel
     *
     * @param array $dados
     * @return objeto Papel
     */
    protected function setDados($dados)
    {
        $papel = new Papel();
        $papel->setId($dados['principal']);
        $papel->setUsuario($dados['usuario']);
        $papel->setSenha($dados['senha']);
        $papel->setNomePapel($dados['nome_papel']);
        $papel->setRotaBase($dados['rota_base']);
        return $papel;
    }
}