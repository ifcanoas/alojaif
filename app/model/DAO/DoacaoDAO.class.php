<?php

/**
 * Classe de modelo referente ao objeto Doacao para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 23-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class DoacaoDAO extends AbstractDAO 
{

    /**
    * Construtor da classe DoacaoDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Doacao::table();
        $this->colunmID = 'id';
        $this->colunms = [     'pessoa_id',
                                'tipo_registro_id',
                                'data_registro',
                                'observacao',
                                'quantidade'
                          ];
    }

    /**
     * Retorna um objeto setado Doacao
     * com objetivo de servir as funções getTabela, getLista e getDoacao
     *
     * @param array $dados
     * @return objeto Doacao
     */
    protected function setDados($dados)
    {
        $doacao = new Doacao();
        $doacao->setId($dados['principal']);
        $doacao->setPessoaId($dados['pessoa_id']);
        $doacao->setTipoRegistroId($dados['tipo_registro_id']);
        $doacao->setDataRegistro($dados['data_registro']);
        $doacao->setObservacao($dados['observacao']);
        $doacao->setQuantidade($dados['quantidade']);
        return $doacao;
    }


    public function getListaCompleta($condicao = false, $order = false, $limit = false, $offset = false)
    {

        $data = $this->queryTable('doacao_completa', 'id as principal, *', $condicao, $order, $limit, $offset);
        $result = [];
        foreach ($data as $linha) {        
            $doacao = $this->setDados($linha);
            $pessoa = new Pessoa();
            $pessoa->setId($linha['pessoa_id']);
            $pessoa->setNomeCompleto($linha['nome_completo']);
            $pessoa->setCpf($linha['cpf']);

            $doacao->setPessoa($pessoa);

            $tipoRegistro = new TipoRegistro();
            $tipoRegistro->setId($linha['tipo_registro_id']);
            $tipoRegistro->setTipo($linha['tipo']);
            
            $doacao->setTipoRegistro($tipoRegistro);

            $result[$linha['principal']] = $doacao;
        }
        return $result;
    }
}