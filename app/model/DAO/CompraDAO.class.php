<?php

/**
 * Classe de modelo referente ao objeto Compra para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 25-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class CompraDAO extends AbstractDAO 
{

    /**
    * Construtor da classe CompraDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Compra::table();
        $this->colunmID = 'id';
        $this->colunms = [     'nota_fiscal',
                                'data_compra',
                                'valor',
                                'quantidade',
                                'item',
                                'observacao',
                                'razao_social_beneficiado',
                                'nome_fantasia'
                          ];
    }

    /**
     * Retorna um objeto setado Compra
     * com objetivo de servir as funções getTabela, getLista e getCompra
     *
     * @param array $dados
     * @return objeto Compra
     */
    protected function setDados($dados)
    {
        $compra = new Compra();
        $compra->setId($dados['principal']);
        $compra->setNotaFiscal($dados['nota_fiscal']);
        $compra->setDataCompra($dados['data_compra']);
        $compra->setValor($dados['valor']);
        $compra->setQuantidade($dados['quantidade']);
        $compra->setItem($dados['item']);
        $compra->setObservacao($dados['observacao']);
        $compra->setRazaoSocialBeneficiado($dados['razao_social_beneficiado']);
        $compra->setNomeFantasia($dados['nome_fantasia']);
        return $compra;
    }
}