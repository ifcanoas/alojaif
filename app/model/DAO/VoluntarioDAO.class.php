<?php

/**
 * Classe de modelo referente ao objeto Voluntario para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 10-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class VoluntarioDAO extends AbstractDAO 
{

    /**
    * Construtor da classe VoluntarioDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Voluntario::table();
        $this->colunmID = 'id';
        $this->colunms = [     'nome_completo',
                                'cpf',
                                'telefone',
                                'foto',
                                'tipo_voluntario_id',
                                'obs',
                          ];
    }

    /**
     * Retorna um objeto setado Voluntario
     * com objetivo de servir as funções getTabela, getLista e getVoluntario
     *
     * @param array $dados
     * @return objeto Voluntario
     */
    protected function setDados($dados)
    {
        $voluntario = new Voluntario();
        $voluntario->setId($dados['principal']);
        $voluntario->setNomeCompleto($dados['nome_completo']);
        $voluntario->setCpf($dados['cpf']);
        $voluntario->setTelefone($dados['telefone']);
        $voluntario->setFoto($dados['foto']);
        $voluntario->setTipoVoluntarioId($dados['tipo_voluntario_id']);
        $voluntario->setObs($dados['obs']);
        return $voluntario;
    }
}