<?php

/**
 * Classe de modelo referente ao objeto Sala para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 10-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class SalaDAO extends AbstractDAO 
{

    /**
    * Construtor da classe SalaDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  Sala::table();
        $this->colunmID = 'id';
        $this->colunms = [     'bloco',
                                'sala',
                                'capacidade'
                          ];
    }

    /**
     * Retorna um objeto setado Sala
     * com objetivo de servir as funções getTabela, getLista e getSala
     *
     * @param array $dados
     * @return objeto Sala
     */
    protected function setDados($dados)
    {
        $sala = new Sala();
        $sala->setId($dados['principal']);
        $sala->setBloco($dados['bloco']);
        $sala->setSala($dados['sala']);
        $sala->setCapacidade($dados['capacidade']);
        return $sala;
    }
}