<?php

/**
 * Classe de modelo referente ao objeto TipoDoacao para 
 * a manutenção dos dados no sistema 
 *
 * @package app.
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 25-05-2024(Gerado automaticamente - GC - 2.0.0 29/08/2023)
 */

class TipoDoacaoDAO extends AbstractDAO 
{

    /**
    * Construtor da classe TipoDoacaoDAO esse metodo  
    * instancia o Modelo padrão conectando o mesmo ao banco de dados
    *
    */
    public function __construct()
    {
        parent::__construct();

        $this->table =  TipoDoacao::table();
        $this->colunmID = 'id';
        $this->colunms = [     'tipo_doacao',
                                'cor'
                          ];
    }

    /**
     * Retorna um objeto setado TipoDoacao
     * com objetivo de servir as funções getTabela, getLista e getTipoDoacao
     *
     * @param array $dados
     * @return objeto TipoDoacao
     */
    protected function setDados($dados)
    {
        $tipoDoacao = new TipoDoacao();
        $tipoDoacao->setId($dados['principal']);
        $tipoDoacao->setTipoDoacao($dados['tipo_doacao']);
        $tipoDoacao->setCor($dados['cor']);
        return $tipoDoacao;
    }
}