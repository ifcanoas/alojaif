<div id="content">
    <table class="tabelaPadrao" id="tabelaPets">
        <colgroup>
            <col span="1" style="width: 10em;">
            <col span="1" style="width: 20em;">
            <col span="1" style="width: 50em;">

        </colgroup>
        <thead>
            <tr>
                <th>Foto</th>
                <th>Tipo</th>
                <th>Descrição</th>
            </tr>
        </thead>
        <tbody>
            {foreach $pets as $pet}
                    <tr>
                        <td>

                {if $pet->getFoto()}
                                    <img src="/public/getMiniaturaPet/{$pet->getId()}" class="miniatura">

                {/if}
                        </td>
                        <td>
                            {$tipos[$pet->getTipoPetId()]->getTipo()|default:'<a href="">Editar</a>'}
                        </td>
                        <td>
                            ID: <a href="/public/pet/ver/{$pet->getId()}" class="linkModal">{$pet->getId()}</a> <br />
                            Nome: {$pet->getNome()} <br />
                            Sexo: {$pet->getSexo()}<br />
                            Descrição: {$pet->getNome()}
                            <p class="text-right"> <a href="/admin/pet/ver/{$pet->getId()}"> Ver mais sobre esse pet</a> </p>
                        </td>
                        
                    </tr>

            {/foreach}
        </tbody>
    </table>
</div>
<footer class="text-right" style="margin-top:5px">
    Desenvolvido por voluntários do Abrigo IFRS Canoas. Colabore em
    <a href="https://gitlab.com/ifcanoas/alojaif" target="_blank">https://gitlab.com/ifcanoas/alojaif</a>
</footer>