

<div id="content">
<table id="tabelaAbrigados">

    <colgroup>
        <col span="1" style="width: 40%;">
        <col span="1" style="width: 20%;">
        <col span="1" style="width: 20%;">
        <col span="1" style="width: 20%;">
    </colgroup>

    <thead id="headTabela">
        <tr>
            <th>NOME</th>
            <th>STATUS</th>
            <th>BAIRRO</th>
            <th>DATA SAÍDA</th>
        </tr>
    </thead>
    <tbody>
        {foreach $abrigados as $abrigado}
            <tr>
                <td class="nome">
                    {$abrigado->getNomeCompleto()}
                </td>
       
                <td>
                    <span id="status{$abrigado->getId()}" 
                                    class="badge {$situacaoFragment->getCor($abrigado->getSituacaoId())}"
                            >{$situacaoFragment->getLabel($abrigado->getSituacaoId())}</span>
                </td>
                <td>{$abrigado->getBairro()}</td>
                <td>{$abrigado->getDataSaidaFormatada()}</td>
            </tr>
        {/foreach}

    </tbody>

</table>


</div>