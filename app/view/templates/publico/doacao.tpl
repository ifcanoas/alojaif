<div id="content">

    <div class="divResponsiva">
        <div id="precisamos">
            <h4>
                O que precisamos
            </h4>
            <div class="divInterna">
                <div class="col">
                    <h5> PRODUTOS DE LIMPEZA  </h5>
                    <p> Nesse momento o que mais precisamos é produtos de limpeza pois as familias estão
                        voltando para casa e precisam muito</p>
                    <p> Também tentamos manter atualizado o espaço no site do 
                        <a href="https://sos-rs.com/abrigo/5935f47f-3932-4cae-affa-df0fbb06f51f" target="_blank">SOS-RS</a> 
                        e em nosso famoso quadro da portaria.
                    </p>
                </div>
            </div>
        </div>
        <div id="doacao">
            <h4>
                O que temos para doar
            </h4>
            <div class="divInterna">
                <ul>
                    <li> Água potável</li>
                    <li> Fraldas (consultar)</li>
                    <li> Alguns itens de higiene (consultar)</li>

                </ul>
            
            </div>
        </div>
    </div>
    <div class="divResponsiva">
        <div id="doadores">
            <h4>Nossos doadores</h4>
            <div class="divInterna"></div>
        </div>
        <div id="prestacao">
            <h4>
                Contas
            </h4>
            <div class="divInterna"></div>
        </div>
    </div>
</div>

<footer class="text-right" style="margin-top:5px">
    Desenvolvido por voluntários do Abrigo IFRS Canoas. Colabore em 
    <a href="https://gitlab.com/ifcanoas/alojaif" target="_blank">https://gitlab.com/ifcanoas/alojaif</a>
</footer>