<div id="content">
    <div id="containerGeral">
        <div id="abrigados">
            <div id="totalAbrigadosDiv">
                <div class="containerGrande">
                    <div class="quadroGrande">
                        <h5 id="totalJaAbrigadosTitle">Total já abrigados</h5>
                        <h3 id="totalJaAbrigados">0</h3>
                    </div>
                    <div class="quadroGrande">
                        <h5 id="totalAbrigadosTitle">Total Abrigados</h5>
                        <h3 id="totalAbrigados">0</h3>
                    </div>
                    <div class="quadroGrande">
                        <h5 id="totalDesocupacoesTitle">Total desocupações</h5>
                        <h3 id="totalDesocupacoes">0</h3>
                    </div>
                </div>
                <div class="containerPequeno">
                    <div class="quadroPequeno">
                        <h5 id="adolescentesTitle">Adolescentes</h5>
                        <p id="adolescentes"></p>
                    </div>
                    <div class="quadroPequeno">
                        <h5 id="adultosTitle">Adultos</h5>
                        <p id="adultos"></p>
                    </div>
                    <div class="quadroPequeno">
                        <h5 id="idososTitle">Idosos</h5>
                        <p id="idosos"></p>
                    </div>
                    <div class="quadroPequeno">
                        <h5 id="semDataTitle">Sem data</h5>
                        <p id="semData"></p>
                    </div>
                </div>
                <div class="quadroPequeno" id="criancasQuadro">
                    <div><h5 id="criançasTitle">Crianças</h5></div>
                    <div class="idades">Total: <b id="criancas">...</b> </div>
                    <div class="idades">0-2 anos:<b id="c0-2">...</b></div>
                    <div class="idades">3-7 anos:<b id="c3-7">...</b></div>
                    <div class="idades">8-12 anos:<b id="c8-12">...</b></div>
                </div>
            </div>
            <div id="totalGeral"></div>
        </div>
        <div id="salas">
            <h5 id="salasTitle">Salas</h5>
            <div class="salaDiv"></div>
        </div>
    </div>
    <h5 id="situacoesTitle">Situações</h5>
    <div id="situacoes">
    </div>

    <h3 style="text-align: center;">Refeições</h3>
    <table id="almoco" class="tabelaPadrao">
        <col span="1" style="width: 33.33%;">
        <col span="1" style="width: 33.33%;">
        <col span="1" style="width: 33.33%;">
        <thead>
            <tr>
                <th>
                    Dia
                </th>
                <th>
                    Almoço
                </th>
                <th>
                    Janta
                </th>
            </tr>
        </thead>
        <tbody>
            {foreach $dias as $key => $dia}
                <td><h4>{$key}</h4></td>
                <td><h4>{$dia['almoco']}</h4></td>
                <td><h4>{$dia['janta']}</h4></td>
                </tr>
            {/foreach}
        </tbody>
    </table>

    <table id="tabelaAbrigados">

        <colgroup>
            <col span="1" style="width: 4em;">
            <col span="1" style="width: 7em;">
            <col span="1" style="width: 30em;">
            <col span="1" style="width: 7em;">
            <col span="1" style="width: 7em;">
            <col span="1" style="width: 7em;">

            <col span="1" style="width: 7em;">
            <col span="1" style="width: 30em;">
        </colgroup>

        <thead id="headTabela">
            <tr>
                <th>ID</th>
                <th>FOTO</th>
                <th>NOME</th>
                <th>IDADE</th>
                <th>SALA</th>
                <th>SITUAÇÃO</th>
                <th>Bairro</th>
                <th>OBSERVAÇÕES</th>
                <th>DATA SAÍDA</th>
                {* 
                    deficiencia
                    mapeamento cidade e bairro
                    registro
                    botao saída definitiva
                    checklist de saída
                *}
            </tr>
        </thead>
        <tbody>
            {* admin/abrigado/editar/?{$abrigado->getID()} *}
            {foreach $abrigados as $abrigado}
                <tr>
                    <td class="id">
                        {$abrigado->getId()}
                        {if $SESSAO['logado'] == 'admin'}
                            <a href="/admin/abrigado/deletarFim/{$abrigado->getId()}">[x]</a>
                        {/if}
                    </td>
                    <td class="foto">
                        {if $abrigado->getFoto()}
                            <a href="/admin/abrigado/getFotoModal/{$abrigado->getId()}" class="modalGeral">
                                <img class="imagemTabela miniatura"
                                    src="/admin/abrigado/getMiniatura/{$abrigado->getId()}" 
                                    width="100%">
                            </a>
                        {/if}
                    </td>
                    <td class="nome">
                        <a href="/admin/abrigado/ver/{$abrigado->getId()}/ajax" class="modalGeral">
                        {$abrigado->getNomeCompleto()}
                        </a>
                        
                    </td>
                    <td class="idade">{$abrigado->getIdade()}</td>
                    <td class="sala">
                        {foreach $salas as $sala}
                            {if $sala->getId() == $abrigado->getSalaId()}<span
                                class="sala{$sala->getId()}">{$sala->getBloco()}{$sala->getSala()}</span>{/if}
                        {/foreach}
                    </td>
                    <td class="situacao">
                        {foreach $situacoes as $situacao}
                            {if $abrigado->getSituacaoId() == $situacao->getId()}<span
                                class="situacao{$situacao->getId()}">{$situacao->getSituacao()}</span>{/if}
                        {/foreach}
                    </td>
                    <td>{$abrigado->getBairro()}</td>

                    <td class="observacao">{$abrigado->getObservacao()}</td>
                    <td>{$abrigado->getDataSaidaFormatada(true,false)}</td>

                </tr>
            {/foreach}

        </tbody>

    </table>
</div>