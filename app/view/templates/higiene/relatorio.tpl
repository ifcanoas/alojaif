    <h5>Produtos de Higiene</h5>
    <ul class="listTable" id="list{7}{$abrigado->getId()}">
        {foreach $registros[7] as $reg}
            {if $reg->getAbrigadoId() == $abrigado->getId()}
            <li> {$reg->getDataRegistroFormatada(false, false)}: {$reg->getObservacao()|truncate:40:"...":true}  </li>
            {/if}
        {/foreach}
    </ul>

<h5>Produtos de limpeza</h5>
<ul class="listTable" id="list{7}{$abrigado->getId()}">
{foreach $registros[6] as $reg}
    {if $reg->getAbrigadoId() == $abrigado->getId()}
    <li> {$reg->getDataRegistroFormatada(false, false)}: {$reg->getObservacao()|truncate:40:"...":true} </li>
    {/if}
{/foreach}
</ul>