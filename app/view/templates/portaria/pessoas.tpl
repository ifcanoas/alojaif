
<div class="wrapper-table">
<table id="tabelaAbrigados">
<colgroup>
<col span="1" style="width: 4em;">
<col span="1" style="width: 7em;">
<col span="1" style="width: 30em;">
</colgroup>
    <thead id="headTabela">
        <tr>
            <th>FOTO</th>
            <th>NOME</th>
            <th>AÇÕES</th>
        </tr>
    </thead>
    <tbody>
        {* admin/abrigado/editar/?{$abrigado->getID()} *}
        {foreach $pessoas as $pessoa}
            <tr id="tr{$pessoa->getId()}">
                <td class="foto">
                    {if !$pessoa->getFoto()}
                        Sem FOTO (mandar para recepção)
                    {else}
                        <a href="/admin/abrigado/getFotoModal/{$pessoa->getId()}" class="modalGeral" >
                            <img class="imagemTabela miniatura" src="/admin/abrigado/getMiniatura/{$pessoa->getId()}" width="100%">
                        </a>
                    {/if}
                </td>
                <td class="id">
                    ID: {$pessoa->getId()} | Sala: {$salas[$pessoa->getSalaId()]->bloco}{$salas[$pessoa->getSalaId()]->sala}<br />
                    Nome:<strong> {$pessoa->getNomeCompleto()} </strong> <br />
                    Idade: {$pessoa->getIdade()} <br />
                    <br />
                    Status: <span   id="status{$pessoa->getId()}" 
                                    class="badge {$situacaoFragment->getCor($pessoa->getSituacaoId())}"
                            >{$situacaoFragment->getLabel($pessoa->getSituacaoId())}</span>
                </td>
                <td class="observacao">
                    <button class="btn btn-danger btnEntrar " 
                        data-id="{$pessoa->getId()}" 
                        data-situacao="{$pessoa->getSituacaoId()}"
                    > <i class="fas fa-sign-in-alt"></i> Entrar</button> 
                    <button class="btn btn-warning btnSair" 
                        data-id="{$pessoa->getId()}"
                        data-situacao="{$pessoa->getSituacaoId()}"
                    ><i class="fas fa-walking"></i> Sair</button>
                    <button class="btn btn-success btnSaidaDefinitiva" 
                        data-id="{$pessoa->getId()}"
                        data-situacao="{$pessoa->getSituacaoId()}"
                    ><i class="fas fa-glass-cheers"></i> Desocupar</button>
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>


</div>

