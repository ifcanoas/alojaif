<button class="btn btn-primary" id="adicionarDoacao">Adicionar doação</button>

<div class="wrapper-table">
    <table id="tabelaDoacoes">
        <colgroup>
            <col span="1" style="width: 15em;">
            <col span="1" style="width: 25em;">
            <col span="1" style="width: 15em;">
            <col span="1" style="width: 10em;">
            <col span="1" style="width: 25em;">
        </colgroup>

        <thead id="headTabela">
            <tr>
                <th>Data</th>
                <th>NOME</th>
                <th>CPF</th>
                <th>CATEGORIA PESSOA</th>
                <th>DOAÇÃO</th>
            </tr>
        </thead>
        <tbody id="tabelaDoacoesData">
            {foreach $doacoes as $doacao}
                <tr>
                    <td>{$doacao->getDataRegistroFormatada(false, false)}</td>
                    <td>{$doacao->getPessoa()->getNomeCompleto()}</td>
                    <td>{$doacao->getPessoa()->getCpf()}</td>
                    <td>{$doacao->getTipoRegistro()->getTipo()}</td>
                    <td>Quantidade: {$doacao->getQuantidade()} {$doacao->getObservacao()}</td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>

<template id="adicionarDoacaoTPL">
    <form action="{$ACAO_FORM}" method="post" id="formDoacao">
        <fieldset id="buscaPessoaTela">
            <input type="number" id="cpf" placeholder="cpf">
            <button id="btnBuscaPessoa" class="btn btn-primary" >Buscar</button>
            <small class="form-text text-muted">Digite apenas números sem espaço e pontos.</small>

        </fieldset>    
        <fieldset id="formPessoa" style="display: none;">
            <legend>Pessoa</legend>
            <input type="hidden" id="pessoaId" name="pessoaId" required  value="-1" />
            <small id="messagePessoa" class="form-text text-muted">Pessoa não encontrada digite os dados.</small>

            <div class="form-group row">
                <div class="col">
                    <label class="control-label" for="tipoPessoaId">Tipo pessoa</label>
                    <select id="tipoPessoaId" name="tipoPessoaId" class="form-control">
                        {html_options options=$listaTipoPessoa selected=5}
                    </select>
                    <a href="/admin/tipoPessoa/criarNovo?return=admin/Pessoa" class="addData">[+] Tipo pessoa</a>
                </div>
           
                <div class="col">
                    <label class="control-label" for="nome">Nome</label>
                    <input type="text" id="nomeCompleto" name="nomeCompleto" class="form-control"  />
                </div>
            </div>
            <div class="form-group row">
                <div class="col">
                <label class="control-label" for="cpf2">CPF</label>
                    <input type="number" id="cpf2" name="cpf" class=" form-control"  />
                    <small class="form-text text-muted">Digite apenas números sem espaço e pontos.</small>
                </div>
                <div class="col">
                    <label class="control-label" for="observacao">Observação</label>
                        <textarea id="observacaoPessoa" name="observacaoPessoa" class="form-control"></textarea>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend>Doação</legend>
            <div class="form-group">
            <label class="control-label col-sm-2" for="tipoRegistroId">Tipo de doação</label>
            <div class="col-sm-8">
                <select id="tipoRegistroId" name="tipoRegistroId" class="form-control">
    		        {html_options options=$listaTipoRegistro}
                </select>
            </div>
        </div>
 
        <div class="form-group">
            <label class="control-label col-sm-2" for="observacao">Observação</label>
            <div class="col-sm-8">
                 <input type="text" id="observacao" name="observacao" class=" form-control"   />
            </div>
        </div>
        </fieldset>    

        {if isset($EXTRA)}
        {include file=$EXTRA}
        {/if}

        <div class="row">
            <div class="col">
                <button class="btn btn-warning" type="reset" id="btnCancelar" >Cancelar tudo</button>
            </div>

            <div class="col text-right">        
                <button class="btn btn-primary" type="submit" disabled="true" id="btnRegistrar" >Registrar doação</button>
            </div>
        </div>
    </form>
</template>