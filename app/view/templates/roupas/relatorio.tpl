<ul class="listTable" id="roupasRelatorio{$abrigado->getId()}">
    {foreach $tiposDeRegistro as $tipoDeRegistro}    
        {if isset($registros[$tipoDeRegistro->getID()][$abrigado->getId()])}
            {foreach $registros[$tipoDeRegistro->getID()][$abrigado->getId()] as $reg}
                <li>{$reg->getQuantidade()} - {$tipoDeRegistro->getTipo()} - {$reg->getDataRegistroFormatada(false, false)}: {$reg->getObservacao()|truncate:40:"...":true} </li>
            {/foreach}
        {/if}
    {/foreach}
</ul>
