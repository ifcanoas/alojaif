<fieldset class="login">
    <h3><i class="fas fa-user-circle"></i> Login</h3>
    <label>
        Usuário
        <input class="text" type="text" name="email" />
    </label>

    <label>
        Senha
        <input class="text" type="password" name="senha" />
    </label>
    <input class="btn btn-primary" type="submit" value="Entrar" />

    <div id="message" style="display: none;">
        <i class="fas fa-exclamation-circle"></i> Login incorreto! Verifique a senha do dia com algum professor de preferência Sandro, Marcio ou Alexandre.
    </div>

</fieldset>





{literal}
    <script>
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };

        if(getUrlParameter('erro') == 'true') {
            console.log('aa');
            document.getElementById('message').style.display = 'block'
        };
    </script>
{/literal}

<footer class="text-center" style="margin-top:5px">
    Desenvolvido por voluntários do Abrigo IFRS Canoas. Colabore em 
    <a href="https://gitlab.com/ifcanoas/alojaif" target="_blank">https://gitlab.com/ifcanoas/alojaif</a>
</footer>