<div id="content">
<a style="margin-bottom: 1rem;" href="/admin/voluntario/criarNovo" class="btn btn-primary">Registrar novo voluntario</a>
<table id="tabelaVoluntario" class="tabelaPadrao">

    <colgroup>
        <col span="1" style="width: 4em;">
        <col span="1" style="width: 7em;">
        <col span="1" style="width: 30em;">
        <col span="1" style="width: 7em;">
        <col span="1" style="width: 7em;">
        <col span="1" style="width: 7em;">
        <col span="1" style="width: 2em;">
    </colgroup>

    <thead id="headTabela">
        <tr>
            <th>ID</th>
            <th>FOTO</th>
            <th>NOME</th>
            <th>CPF</th>
            <th>TELEFONE</th>
            <th>TIPO</th>
            <th>OBS</th>
        </tr>
    </thead>
    <tbody>
        {* admin/abrigado/editar/?{$abrigado->getID()} *}
        {foreach $voluntarios as $voluntario}
            <tr>
                <td class="id">{$voluntario->getId()}</td>
                <td class="foto">
                    {if $voluntario->getFoto()}
                        <a href="/admin/voluntario/getFoto/{$voluntario->getId()}" target="_blank"><img class="imagemTabela"
                                src="/admin/voluntario/getMiniatura/{$voluntario->getId()}" width="100%"></a>
                    {else}
                        <a href="/admin/voluntario/editarModal/{$voluntario->getId()}"
                            class="modalGeral">Tirar Foto
                        </a>
                    {/if}
                </td>
                <td class="nome">
                    <a href="/admin/voluntario/editarModal/{$voluntario->getId()}"
                        class="modalGeral">{$voluntario->getNomeCompleto()}
                    </a>
                </td>
                <td>{$voluntario->getCpf()}</td>
                <td>
                    {$voluntario->getTelefone()}
                </td>
                <td>
                    {$tipos[$voluntario->getTipoVoluntarioId()]}
                </td>

                <td class="obs">{$voluntario->getObs()}</td>
            </tr>
        {/foreach}

    </tbody>

</table>
</div>