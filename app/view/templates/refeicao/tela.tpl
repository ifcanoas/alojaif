
    <div class="wrapper-table">
        <div>
        <h1>Registro de refeição de: {$dataAtualVisual}</h1>  
        <p>
            <br/>
            Quantidade de almoços servidos: {$quantidadeAlmoco}
            <br/>
            Quantidade de jantas servidas: {$quantidadeJanta}
        </p>

        <a class="btn btn-primary" href="/admin/abrigado/estatisticasRefeicao">
            Ver todos os dias
        </a>
        </div>
        <table id="tabelaAbrigados">

            <colgroup>
                <col span="1" style="width: 4em;">
                <col span="1" style="width: 7em;">
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 35em;">
                <col span="1" style="width: 35em;">
            </colgroup>

            <thead id="headTabela">
                <tr>
                    <th>ID</th>
                    <th>FOTO</th>
                    <th>NOME</th>
                    <th>SALA</th>
                    <th>ALMOÇO</th>
                    <th>JANTA</th>
                </tr>
            </thead>
            <tbody>
                {assign var="i" value=1}
                {foreach $abrigados as $abrigado}
                    <tr>
                        <td class="id">{$abrigado->getId()}</td>
                        <td class="foto">
                            {if $abrigado->getFoto()}
                                <a href="/admin/abrigado/getFotoModal/{$abrigado->getId()}" class="linkModal" >
                                    <img class="imagemTabela" src="/admin/abrigado/getMiniatura/{$abrigado->getId()}" width="100%">
                                </a>
                            {/if}
                        </td>
                        <td class="nome">
                            {$abrigado->getNomeCompleto()}
                        </td>
                        <td>
                        
                        </td>
                        <td class="almoco">
                            <div>
                                {assign var="registroAlmoco" value=$abrigado->getRegistro($registrosAlmoco)}
                                <input id="almoco{$i}" onchange="cadastrarRegistro({$abrigado->getId()}, 3, '', {$dataAtual}, 'almoco{$i}')" {if $registroAlmoco !== null} data-id="{$registroAlmoco->getId()}" {else} data-id="-1" {/if} type="checkbox" name="registroAlmoco" {if $registroAlmoco !== null} checked {/if} />
                            </div>
                        </td>
                        <td class="janta">
                            <div>
                                {assign var="registroJanta" value=$abrigado->getRegistro($registrosJanta)}
                                <input id="janta{$i}" onchange="cadastrarRegistro({$abrigado->getId()}, 4, '', {$dataAtual}, 'janta{$i++}')" {if $registroJanta !== null} data-id="{$registroJanta->getId()}" {else} data-id="-1" {/if} type="checkbox" name="registroJanta" {if $registroJanta !== null} checked {/if} />
                            </div>   
                        </td>
                    </tr>
                {/foreach}

            </tbody>

        </table>

        
    </div>