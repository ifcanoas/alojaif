{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Tipo pessoa</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$tipoPessoa->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="tipoPessoa">Tipo pessoa</label>
            <div class="col-sm-8">
                 <input type="text" id="tipoPessoa" name="tipoPessoa" class=" form-control"  value="{$tipoPessoa->getTipoPessoa()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="cor">Cor</label>
            <div class="col-sm-8">
                 <input type="text" id="cor" name="cor" class=" form-control"  value="{$tipoPessoa->getCor()}" />
            </div>
        </div>
</fieldset>

