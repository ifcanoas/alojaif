{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Compra</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$compra->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="notaFiscal">Nota fiscal</label>
            <div class="col-sm-8">
                 <input type="text" id="notaFiscal" name="notaFiscal" class=" form-control"  value="{$compra->getNotaFiscal()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="dataCompra">Data compra</label>
            <div class="col-sm-8">
                 <input type="date" id="dataCompra" name="dataCompra" class=" form-control" required  value="{$compra->getDataCompra()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="valor">Valor</label>
            <div class="col-sm-8">
                 <input type="text" id="valor" name="valor" class="validaFloat form-control" required  value="{$compra->getValor()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="quantidade">Quantidade</label>
            <div class="col-sm-8">
                 <input type="text" id="quantidade" name="quantidade" class="validaInteiro form-control"  value="{$compra->getQuantidade()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="item">Item</label>
            <div class="col-sm-8">
                 <input type="text" id="item" name="item" class=" form-control"  value="{$compra->getItem()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="observacao">Observação</label>
            <div class="col-sm-8">
                 <input type="text" id="observacao" name="observacao" class=" form-control"  value="{$compra->getObservacao()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="razaoSocialBeneficiado">Razao social beneficiado</label>
            <div class="col-sm-8">
                 <input type="text" id="razaoSocialBeneficiado" name="razaoSocialBeneficiado" class=" form-control"  value="{$compra->getRazaoSocialBeneficiado()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="nomeFantasia">Nome fantasia</label>
            <div class="col-sm-8">
                 <input type="text" id="nomeFantasia" name="nomeFantasia" class=" form-control"  value="{$compra->getNomeFantasia()}" />
            </div>
        </div>
</fieldset>

