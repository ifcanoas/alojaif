{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Sala</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$sala->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="bloco">Bloco</label>
            <div class="col-sm-8">
                 <input type="text" id="bloco" name="bloco" class=" form-control"  value="{$sala->getBloco()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="sala">Sala</label>
            <div class="col-sm-8">
                 <input type="text" id="sala" name="sala" class=" form-control"  value="{$sala->getSala()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="capacidade">Capacidade</label>
            <div class="col-sm-8">
                 <input type="text" id="capacidade" name="capacidade" class="validaFloat form-control"  value="{$sala->getCapacidade()}" />
            </div>
        </div>
</fieldset>

