{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Presenca</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$presenca->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="voluntarioId">Voluntario id</label>
            <div class="col-sm-8">
                 <select id="voluntarioId" name="voluntarioId" class="form-control">
    		{html_options options=$listaVoluntario selected=$presenca->getVoluntarioId()}
             </select>
<a href="/admin/voluntario/criarNovo?return=admin/Presenca" class="addData">[+] Voluntario</a>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="presencaId">Presenca id</label>
            <div class="col-sm-8">
                 <select id="presencaId" name="presencaId" class="form-control">
    		{html_options options=$listaPresenca selected=$presenca->getPresencaId()}
             </select>
<a href="/admin/presenca/criarNovo?return=admin/Presenca" class="addData">[+] Presenca</a>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="dataRegistro">Data registro</label>
            <div class="col-sm-8">
                 <input type="text" id="dataRegistro" name="dataRegistro" class=" form-control"  value="{$presenca->getDataRegistro()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="entrada">Entrada</label>
            <div class="col-sm-8">
                 <input type="text" id="entrada" name="entrada" class=" form-control"  value="{$presenca->getEntrada()}" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="saida">Saida</label>
            <div class="col-sm-8">
                 <input type="text" id="saida" name="saida" class=" form-control"  value="{$presenca->getSaida()}" />
            </div>
        </div>
</fieldset>

