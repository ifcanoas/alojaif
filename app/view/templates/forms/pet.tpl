{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Pet</legend>
    <input type="hidden" id="id" name="id" class=" form-control" required value="{$pet->getId()}" />
    <div class="form-group">
        <label class="control-label col-sm-2" for="abrigadoId">Tutor/Abrigado</label>
        <div class="row"  id="buscaTutor">
            <div class="col">
                <input type="text" class="form-control" id="inputTutor">
                <small id="buscaErro" class="form-text text-danger" ></small>

            </div>
            <div class="col">
                <button id="btnBuscaTutor" class="btn btn-primary">Buscar Tutor</button>
            </div>
        </div>
        <div id="telaTutorSelecionado" style="display: none;">
            <div class="row"> 
                <div class="col-4">
                    <button id="btnRemoverTutor" class="btn btn-primary">[x] Remover Tutor</button>
                </div>
                <div id="tutor" class="col-8">
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="nome">Nome</label>
        <div class="col-sm-8">
            <input type="text" id="nome" name="nome" class=" form-control" value="{$pet->getNome()}" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="raca">Raça</label>
        <div class="col-sm-8">
            <input type="text" id="raca" name="raca" class=" form-control" value="{$pet->getRaca()}" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="descricao">Descrição</label>
        <div class="col-sm-8">
            <textarea id="descricao" name="descricao" class=" form-control">{$pet->getDescricao()}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="foto">Foto</label>
        <div class="col-sm-8">
            <input type="file" id="foto" name="foto" class=" form-control" value="{$pet->getFoto()}" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="sexo">Sexo</label>
        <div class="col-sm-8">
            <select id="sexo" name="sexo" class=" form-control">
                <option value="macho" {if $pet->getSexo() == 'macho'} selected{/if}>Macho</option>
                <option value="fêmea" {if $pet->getSexo() == 'fêmea'} selected{/if}>Fêmea</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="vacinas">Vacinas</label>
        <div class="col-sm-8">
            <input type="checkbox" id="vacinas" name="vacinas" class=" form-control" value="true"
                {if $pet->getVacinas()} checked="checked" {/if} />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="tipoPetId">Tipo pet id</label>
        <div class="col-sm-8">
            <select id="tipoPetId" name="tipoPetId" class="form-control">
                {html_options options=$listaTipoPet selected=$pet->getTipoPetId()}
            </select>

            <a href="/admin/tipoPet/criarNovo?return=admin/Pet" class="addData">[+] Tipo pet</a>

        </div>
    </div>
</fieldset>