{*Gerado automaticamente com GC - 2.0.0 29/08/2023*}
<fieldset class="formPadrao">
    <legend>Responsavel</legend>
             <input type="hidden" id="id" name="id" class=" form-control" required  value="{$responsavel->getId()}" />
        <div class="form-group">
            <label class="control-label col-sm-2" for="abrigadoId">Abrigado id</label>
            <div class="col-sm-8">
                 <select id="abrigadoId" name="abrigadoId" class="form-control">
    		{html_options options=$listaAbrigado selected=$responsavel->getAbrigadoId()}
             </select>
<a href="/admin/abrigado/criarNovo?return=admin/Responsavel" class="addData">[+] Abrigado</a>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="responsavelId">Responsavel id</label>
            <div class="col-sm-8">
                 <select id="responsavelId" name="responsavelId" class="form-control">
    		{html_options options=$listaResponsavel selected=$responsavel->getResponsavelId()}
             </select>
<a href="/admin/responsavel/criarNovo?return=admin/Responsavel" class="addData">[+] Responsavel</a>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="tipoResponsavelId">Tipo responsavel id</label>
            <div class="col-sm-8">
                 <select id="tipoResponsavelId" name="tipoResponsavelId" class="form-control">
    		{html_options options=$listaTipoResponsavel selected=$responsavel->getTipoResponsavelId()}
             </select>
<a href="/admin/tipoResponsavel/criarNovo?return=admin/Responsavel" class="addData">[+] Tipo responsavel</a>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="observacao">Observação</label>
            <div class="col-sm-8">
                 <input type="text" id="observacao" name="observacao" class=" form-control"  value="{$responsavel->getObservacao()}" />
            </div>
        </div>
</fieldset>

