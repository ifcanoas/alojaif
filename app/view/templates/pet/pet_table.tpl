<table border="1" id="tabelaPets">

<colgroup>
    <col span="1" style="width: 30em;">
    <col span="1" style="width: 30em;">
    <col span="1" style="width: 30em;">
</colgroup>

<thead>
    <tr>
        <th>Tipo</th>
        <th>Nome</th>
        <th>Foto</th>
    </tr>
</thead>
<tbody>
{foreach $pets as $pet}
    {include file="pet/pet_row.tpl"}
{/foreach}
</tbody>
</table>