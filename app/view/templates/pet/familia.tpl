<tr>

    <td colspan="8">

        <table border="1" id="tabelaPets">

            <colgroup>
                <col span="1" style="width: 15em;">
                <col span="1" style="width: 30em;">
                <col span="1" style="width: 30em;">
            </colgroup>

            <thead>
                <tr>
                    <th>Foto</th>
                    <th>Tipo</th>
                    <th>Nome</th>
                </tr>
            </thead>
            <tbody>

                {foreach $familia as $abrigado}
                    {if isset($pets[$abrigado->getId()])}
                        {foreach $pets[$abrigado->getId()] as $pet}
                            {include file="pet/pet_row.tpl"}
                        {/foreach}
                    {/if}
                {/foreach}

            <tbody>
        </table>
                

</td>
</tr>