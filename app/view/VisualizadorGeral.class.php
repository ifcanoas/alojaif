<?php

/**
 * Description of VisualizadorGeral
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package 
 */
class VisualizadorGeral extends AbstractView
{

    private $mostrarNavegacao = false;

    public function __construct()
    {
        parent::__construct();
        $this->addTemplate('menu');
        $this->CDN()->add('jquery');
        $this->CDN()->add('bootstrap');
        $this->attValue('SESSAO', $_SESSION);

        $this->addCSS('layout');
        $this->addJS('menu');
        $this->addLibJS('all', '/vendor/@fortawesome/fontawesome-free/js/');


        $this->attValue('navegacaoMenu', $this->mostrarNavegacao);
       
        $this->defineLanguage();
    }

    public function naoMostrarNavegacao()
    {
        $this->attValue('navegacaoMenu', false);
    }

    public function renderAjax()
    {
        unset($this->templates[0]);
        $this->attValue('selfScript', $this->varsJS . PHP_EOL . $this->selfScript);
        $this->attValue('selfScriptPos', $this->selfScriptPos);


        parent::renderAjax();
    }

    public function renderIframe()
    {
        unset($this->templates[0]);
        parent::render();
    }

    private function verificaOffline()
    {
        if (file_exists(ROOT . '../.offline')) {
            return true;
        }
        return false;
    }
  
    public function render()
    {
//        Logger::log(); //Loga apenas em tela grande as funções ajax precisam ser forçadas.        
        Componente::load('Modal');
        $modal = (new Modal('modalGeral'))
            ->setDefaultClassLink('modalGeral')
            ->setTitulo('');
        $this->addComponente($modal);
        parent::render();
    }
}
