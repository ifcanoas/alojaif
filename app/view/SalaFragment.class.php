<?php

class SalaFragment
{

    private $listaSalas;

    public function __construct($listaSalas = false)
    {
        if(!$listaSalas){
            $listaSalas = Sala::getMap();
        }
        $this->listaSalas = $listaSalas;
    }

    public function getCor($id)
    {
        return $this->listaSalas[$id]->getCor();
    }

    public function getLabel($id)
    {
        return $this->listaSalas[$id]->getBloco() . $this->listaSalas[$id]->getSala() ;
    }
}
